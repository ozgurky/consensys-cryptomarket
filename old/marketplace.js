const MarketPlace = artifacts.require("MarketPlace");
const Helper = require("../src/utils/helpers");
const Web3 = require("web3");
const web3Latest = new Web3(web3.currentProvider);

contract('MarketPlace', async (accounts) => {
    const alice = accounts[0];
    const bob = accounts[1];
    const mark = accounts[2];

    let deployedMarketPlace;

    beforeEach(async () => {
        deployedMarketPlace = await MarketPlace.new();
    });

    it('should create an admin', async () => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        const adminData = await deployedMarketPlace.admins(bob);

        assert.equal("Bob", adminData[1], 'First admin name was not Bob');
    });

    it('should create an admin by only super admin', async () => {
        try {
            await deployedMarketPlace.createAdmin(bob, 'Bob', {from: mark});
        } catch (err) {
            assert(true);
        }
    });

    it('should create same admin only once', async() => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        const adminCount = await deployedMarketPlace.getAdminsCount();

        assert.equal("1", adminCount.toString(), 'Same address can be created twice');
    });

    it('should delete an admin with address', async() => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createAdmin(mark, 'Mark');
        await deployedMarketPlace.deleteAdmin(bob)
        const adminCount = await deployedMarketPlace.getAdminsCount();
        const adminData = await deployedMarketPlace.admins(bob);

        assert(!adminData[2], 'Bob admin data status was not false')
        assert.equal("1", adminCount.toString(), 'Admin count was not 1');
    });

    it('should check if user is super admin', async() => {
        assert(await deployedMarketPlace.isSuperAdmin());
        assert(!await deployedMarketPlace.isSuperAdmin({from: mark}));
    });

    it('should check if user is admin', async() => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');

        assert(!await deployedMarketPlace.isAdmin());
        assert(await deployedMarketPlace.isAdmin({from: bob}));
    });

    it('should get user role', async() => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        let userRoleAlice = await deployedMarketPlace.getUserRolesArray();
        userRoleAlice = userRoleAlice.map(function (item) {
            return web3.toDecimal(item);
        });
        assert(Helper.isSuperAdmin(userRoleAlice), "Alice's role was not super admin.");
        assert(!Helper.isAdmin(userRoleAlice), "Alice's role was admin.");
        assert(!Helper.isStoreOwner(userRoleAlice), "Alice's role was store owner.");
        assert(Helper.isUser(userRoleAlice), "Alice's role was not user.");
    });

    /**
     * Store Owner create, delete tests
     */
    it('should create a store owner', async () => {
        //only admins can create store owner so we should create an admin first
        await deployedMarketPlace.createAdmin(bob, 'Bob');

        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        const ownerData = await deployedMarketPlace.storeOwners(mark);

        assert.equal("Mark", ownerData[1], 'First store owner\'s name was not Bob');
    });

    it('should create a store owner by only admin', async () => {
        try {
            await deployedMarketPlace.createStoreOwner(bob, 'Bob', {from: mark});
        } catch (err) {
            assert(true);
        }
    });

    it('should create same store owner only once', async() => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        const ownersCount = await deployedMarketPlace.getStoreOwnersCount();

        assert.equal("1", ownersCount.toString(), 'Same owner address can be created twice');
    });

    it('should delete a store owner with address', async() => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreOwner(alice, 'Alice', {from: bob});

        await deployedMarketPlace.deleteStoreOwner(mark, {from: bob});
        const ownerCount = await deployedMarketPlace.getStoreOwnersCount();
        const ownerData = await deployedMarketPlace.storeOwners(mark);

        assert(!ownerData[2], 'Mark\'s store owner data status was not false')
        assert.equal("1", ownerCount.toString(), 'Store owners count was not 1');
    });

    /**
     * Store Front creation tests
     */
    it('should create a store front', async () => {
        //only admins can create store owner so we should create an admin first
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        const storeFrontData = await deployedMarketPlace.storeFronts(1);

        assert.equal("My Store Front", storeFrontData[2], 'First store front\'s name was not "My Store Front"');
    });

    it('should create a store front only by a store owner', async () => {
        try {
            await deployedMarketPlace.createStoreFront('Store front', {from: mark});
        } catch (err) {
            assert(true);
        }
    });

    it('should update a store front', async () => {
        //only admins can create store owner so we should create an admin first
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.updateStoreFront(1, 'My Updated Store Front', {from: mark});

        const storeFrontData = await deployedMarketPlace.storeFronts(1);

        assert.equal("My Updated Store Front", storeFrontData[2], 'First store front\'s name was not updated.');
    });


    it('should get all store fronts', async () => {
        //only admins can create store owner so we should create an admin first
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front 1', {from: mark});
        await deployedMarketPlace.createStoreFront('My Store Front 2', {from: mark});

        const storeFrontKeysMark = await deployedMarketPlace.getStoreFrontsByAddress(mark);
        const storeFrontKeysAlice = await deployedMarketPlace.getStoreFrontsByAddress(alice);

        assert.equal("2", storeFrontKeysMark.length, 'Mark\'s store fronts count was not 2.');
        assert.equal("0", storeFrontKeysAlice.length, 'Alice\'s store fronts count was not 0.');
    });


    /**
     * Product creation tests
     */
    it('should create a product', async () => {
        //only admins can create store owner so we should create an admin first
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.createProduct(
                1,
                'my product',
                'my product desc',
                1000,
                10,
                true,
                {from: mark}
            );

        const productData = await deployedMarketPlace.products(1);

        assert.equal("my product", productData[2], 'First product\'s name was not "my product"');
    });

    it('should update a product', async () => {
        //only admins can create store owner so we should create an admin first
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.createProduct(
            1,
            'my product',
            'my product desc',
            1000,
            10,
            true,
            {from: mark}
        );

        await deployedMarketPlace.updateProduct(
            1,
            'my product updated',
            'my product desc',
            1000,
            10,
            true,
            {from: mark}
        );

        const productData = await deployedMarketPlace.products(1);

        assert.equal("my product updated", productData[2], 'First product\'s name was not "my product updated"');
    });

    it('should delete a product by id', async () => {
        //only admins can create store owner so we should create an admin first
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 1', 'some desc', 1000, 10, true, {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 2', 'some desc', 1000, 10, true, {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 3', 'some desc', 1000, 10, true, {from: mark});

        await deployedMarketPlace.deleteProduct(2, {from: mark});


        const deletedProduct = await deployedMarketPlace.products(2);

        assert.equal('', deletedProduct[2], 'Product 2 could not be deleted');
    });


    /**
     * Product buy tests
     */
    it('should buy a product with exact value', async () => {
        //only admins can create store owner so we should create an admin first
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 1', 'some desc', 1500, 10, true, {from: mark});

        await deployedMarketPlace.buyProduct(1, {from: alice, value:1500});

        const product = await deployedMarketPlace.products(1);
        const markBalance = await deployedMarketPlace.getSenderBalance({from: mark});

        assert.equal(9, product[5], 'Product quantity was not 9');
        assert.equal(1500, markBalance, 'Mark\'s balance was not 1500');
    });

    it('should buy a product with bigger value & and return extra', async () => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 1', 'some desc', 1500, 10, true, {from: mark});

        await deployedMarketPlace.buyProduct(1, {from: alice, value:15000});

        const product = await deployedMarketPlace.products(1);
        const markBalance = await deployedMarketPlace.getSenderBalance({from: mark});
        const contractBalance = await web3Latest.eth.getBalance(deployedMarketPlace.address);

        assert.equal(9, product[5], 'Product quantity was not 9');
        assert.equal(1500, markBalance, 'Mark\'s balance was not 1500');
        assert.equal(1500, contractBalance.valueOf(), 'Contract balance was not 0');
    });

    it('should buy a product and save details to transaction', async () => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 1', 'some desc', 1500, 10, true, {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 2', 'some desc', 1500, 10, true, {from: mark});

        await deployedMarketPlace.buyProduct(1, {from: alice, value:1500});
        await deployedMarketPlace.buyProduct(2, {from: alice, value:1500});

        const transaction = await deployedMarketPlace.transactions(1);
        const keys = ['id', 'productId', 'productName', 'buyer', 'seller', 'price', 'quantity', 'date'];
        let transactionObject = {};
        keys.map((key, index) => {
            transactionObject[key] = transaction[index].valueOf();
        });

        const aliceTransactions = await deployedMarketPlace.getPurchasedProductsAsBuyer({from: alice});
        const markTransactions = await deployedMarketPlace.getPurchasedProductsAsSeller({from: mark});

        assert.equal(mark, transactionObject.seller, "Transaction seller was not Mark.");
        assert.equal(alice, transactionObject.buyer, "Transaction buyer was not Alice.");
        assert.equal(2, aliceTransactions.length, "Alice\'s transaction count was not 1.");
        assert.equal(2, markTransactions.length, "Mark\'s transaction count was not 1.");
    });

    it('should cause a decrease on buyer balance', async () => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 1', 'some desc', web3Latest.utils.toWei('0.1', 'ether'), 10, true, {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 2', 'some desc', web3Latest.utils.toWei('0.2', 'ether'), 10, true, {from: mark});


        const aliceInitialBalance = await web3Latest.eth.getBalance(alice);
        await deployedMarketPlace.buyProduct(1, {from: alice, value: web3Latest.utils.toWei('0.1', 'ether')});
        await deployedMarketPlace.buyProduct(2, {from: alice, value: web3Latest.utils.toWei('0.2', 'ether')});

        const aliceFinalBalance = await web3Latest.eth.getBalance(alice);
        const difference = aliceInitialBalance - aliceFinalBalance;

        assert(difference > web3Latest.utils.toWei('0.3', 'ether'));
    });

    it('should withdraw from balance', async () => {
        await deployedMarketPlace.createAdmin(bob, 'Bob');
        await deployedMarketPlace.createStoreOwner(mark, 'Mark', {from: bob});
        await deployedMarketPlace.createStoreFront('My Store Front', {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 1', 'some desc', web3Latest.utils.toWei('0.1', 'ether'), 10, true, {from: mark});
        await deployedMarketPlace.createProduct(1, 'my product 2', 'some desc', web3Latest.utils.toWei('0.2', 'ether'), 10, true, {from: mark});


        const markInitialBalance = await web3Latest.eth.getBalance(mark);
        await deployedMarketPlace.buyProduct(1, {from: alice, value: web3Latest.utils.toWei('0.1', 'ether')});
        await deployedMarketPlace.buyProduct(2, {from: alice, value: web3Latest.utils.toWei('0.2', 'ether')});


        await deployedMarketPlace.withdraw({from: mark});

        const markFinalBalance = await web3Latest.eth.getBalance(mark);
        const difference = markFinalBalance - markInitialBalance;

        const markBalanceOnContract = await deployedMarketPlace.getSenderBalance({from: mark});
        assert.equal(0, markBalanceOnContract.valueOf(), "Mark balance was not 0 on contract");
        assert(difference > web3Latest.utils.toWei('0.28', 'ether'), 'Diffence was not bigger than 0.28 ether' + difference);
    });
});