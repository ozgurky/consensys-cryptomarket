pragma solidity ^0.4.24;

contract MarketPlace {
    address public superAdmin;

    struct Admin {
        uint id;
        string name;
        bool status;
    }
    mapping (address => Admin) public admins;
    address[] public adminIndex;

    struct StoreOwner {
        uint id;
        string name;
        bool status;
    }
    mapping (address => StoreOwner) public storeOwners;
    address[] public storeOwnerAddresses;

    uint storeFrontIndex = 1;
    struct StoreFront {
        uint id;
        address owner;
        string name;
        bool status;
    }
    mapping (address => uint[]) public storeFrontsToOwners;
    mapping (uint => StoreFront) public storeFronts;

    struct Product {
        uint id;
        address owner;
        string name;
        string description;
        uint price;
        uint quantity;
        bool status;
    }
    uint public productIndex = 1;
    mapping(uint => Product) public products;
    mapping(uint => uint[]) public productsToStoreFronts;

    mapping(address => uint) balances;
    uint transactionIndex = 1;
    struct Transaction {
        uint id;
        uint productId;
        string productName;
        address buyer;
        address seller;
        uint price;
        uint quantity;
        uint date;
    }
    mapping(address=>uint[]) public buyerTransactions;
    mapping(address=>uint[]) public sellerTransactions;
    mapping(uint => Transaction) public transactions;

    event AdminCreated(address adminAddress, string name, uint id);
    event AdminDeleted(address adminAddress);

    event StoreOwnerCreated(address storeOwnerAddress, string name, uint id);
    event StoreOwnerDeleted(address storeOwnerAddress);

    event StoreFrontCreated(address storeOwnerAddress, string name, uint id);
    event StoreFrontUpdated(uint id, string name);

    event ProductCreated(
        uint id,
        address ownerAddress,
        uint storeFrontId,
        string name,
        uint price,
        uint quantity,
        bool status
    );

    event ProductUpdated(
        uint id,
        address ownerAddress,
        uint productId,
        string name,
        uint price,
        uint quantity,
        bool status
    );

    event ProductDeleted(uint productId);

    event ProductSold(uint id);

    event BalanceWithdrawn(address owner, uint amount);

    modifier onlySuperAdmin {
        require(msg.sender == superAdmin);
        _;
    }

    modifier onlyAdmins {
        require(admins[msg.sender].status == true);
        _;
    }

    modifier onlyStoreOwners {
        require(storeOwners[msg.sender].status == true);
        _;
    }

    constructor () public {
        superAdmin = msg.sender;
    }

    function createAdmin(address _address, string _name) public onlySuperAdmin {
        if (admins[_address].status == true) {
            return;
        }

        uint adminId = adminIndex.push(_address) - 1;

        admins[_address] = Admin({
            id: adminId,
            name: _name,
            status: true
            });

        emit AdminCreated(_address, _name, adminId);
    }

    function deleteAdmin(address _address) public onlySuperAdmin {
        uint rowToDelete = admins[_address].id;
        address keyToMove = adminIndex[adminIndex.length - 1];
        adminIndex[rowToDelete] = keyToMove;
        admins[_address].status = false;
        admins[keyToMove].id = rowToDelete;
        adminIndex.length--;

        emit AdminDeleted(_address);
    }

    function getAdmins() public view returns(address[]) {
        return adminIndex;
    }

    function getAdminsCount() public view returns (uint) {
        return adminIndex.length;
    }

    function createStoreOwner(address _address, string _name) public onlyAdmins {
        if (storeOwners[_address].status == true){
            return;
        }

        uint storeOwnerId = storeOwnerAddresses.push(_address) - 1;

        storeOwners[_address] = StoreOwner({
            id: storeOwnerId,
            name: _name,
            status: true
            });

        emit StoreOwnerCreated(_address, _name, storeOwnerId);
    }

    function deleteStoreOwner(address _address) public onlyAdmins {
        uint rowToDelete = storeOwners[_address].id;
        address keyToMove = storeOwnerAddresses[storeOwnerAddresses.length - 1];
        storeOwnerAddresses[rowToDelete] = keyToMove;
        storeOwners[_address].status = false;
        storeOwners[keyToMove].id = rowToDelete;
        storeOwnerAddresses.length--;

        emit StoreOwnerDeleted(_address);
    }

    function getStoreOwners() public view returns(address[]) {
        return storeOwnerAddresses;
    }

    function getStoreOwnersCount() public view returns (uint) {
        return storeOwnerAddresses.length;
    }

    function createStoreFront(string _name) public onlyStoreOwners {
        uint id = storeFrontIndex++;
        storeFronts[id] = StoreFront({
            id: id,
            owner: msg.sender,
            name: _name,
            status: true
            });

        storeFrontsToOwners[msg.sender].push(id);
        emit StoreFrontCreated(msg.sender, _name, id);
    }

    function updateStoreFront(uint _id, string _name) public onlyStoreOwners {
        require(storeFronts[_id].owner == msg.sender);
        storeFronts[_id].name = _name;
        emit StoreFrontUpdated(_id, _name);
    }

    function getStoreFrontsByOwner() public view returns(uint[]) {
        return storeFrontsToOwners[msg.sender];
    }

    function getStoreFrontsByAddress(address _address) public view returns(uint[]) {
        return storeFrontsToOwners[_address];
    }

    function  createProduct(uint _storeFrontId, string _name, string _description, uint _price, uint _quantity, bool _status)
    public onlyStoreOwners {
        require(storeFronts[_storeFrontId].owner == msg.sender);
        uint productId = productIndex++;

        products[productId] = Product({
            id: productId,
            owner: msg.sender,
            name: _name,
            description: _description,
            price: _price,
            quantity: _quantity,
            status: _status
            });
        productsToStoreFronts[_storeFrontId].push(productId);

        emit ProductCreated(productId, msg.sender, _storeFrontId, _name, _price, _quantity, _status);
    }

    function  updateProduct(uint _productId, string _name, string _description, uint _price, uint _quantity, bool _status)
    public onlyStoreOwners {
        require(products[_productId].owner == msg.sender);

        products[_productId].name = _name;
        products[_productId].description = _description;
        products[_productId].price = _price;
        products[_productId].quantity = _quantity;
        products[_productId].status = _status;


        emit ProductUpdated(_productId, msg.sender, _productId, _name, _price, _quantity, _status);
    }

    function deleteProduct(uint _productId) public onlyStoreOwners {
        require(products[_productId].owner == msg.sender);
        delete products[_productId];
        emit ProductDeleted(_productId);
    }

    function getProductsByStoreFrontId(uint _storeFrontId) public view returns(uint[]) {
        return productsToStoreFronts[_storeFrontId];
    }

    function buyProduct(uint _productId) public payable {
        Product storage product = products[_productId];
        require(product.status && msg.value >= product.price && product.quantity > 0);
        uint extraValue = msg.value - product.price;
        product.quantity--;
        uint currentBalance = balances[product.owner];
        balances[product.owner] = currentBalance + product.price;
        msg.sender.transfer(extraValue);

        uint transactionId = transactionIndex++;
        transactions[transactionId] = Transaction({
            id: transactionId,
            productId: _productId,
            productName: product.name,
            buyer: msg.sender,
            seller: product.owner,
            price: product.price,
            quantity: 1,
            date: now
            });

        buyerTransactions[msg.sender].push(transactionId);
        sellerTransactions[product.owner].push(transactionId);

        emit ProductSold(_productId);
    }

    function getPurchasedProductsAsSeller() public view returns(uint[]) {
        return sellerTransactions[msg.sender];
    }

    function getPurchasedProductsAsBuyer() public view returns(uint[]) {
        return buyerTransactions[msg.sender];
    }

    function getSenderBalance() public view returns(uint) {
        return balances[msg.sender];
    }

    function withdraw() public {
        require(balances[msg.sender] >= 0);
        uint amount = balances[msg.sender];
        balances[msg.sender] = 0;
        msg.sender.transfer(amount);
        emit BalanceWithdrawn(msg.sender, amount);
    }

    function isSuperAdmin() public view returns(bool) {
        if (msg.sender == superAdmin) {
            return true;
        }

        return false;
    }

    function isAdmin() public view returns(bool) {
        return admins[msg.sender].status == true;
    }

    function isStoreOwner() public view returns(bool) {
        return storeOwners[msg.sender].status == true;
    }

    function getUserRolesArray() public view returns(uint[4]) {
        uint[4] memory rolesArray;
        if (msg.sender == superAdmin) {
            rolesArray[0] = 1;
        }
        if (admins[msg.sender].status) {
            rolesArray[1] = 1;
        }
        if (storeOwners[msg.sender].status) {
            rolesArray[2] = 1;
        }

        rolesArray[3] = 1;
        return rolesArray;
    }
}