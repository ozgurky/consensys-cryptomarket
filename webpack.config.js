var path = require("path");
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ],
                exclude: /node_modules/,
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [ 'file-loader', {
                    loader: 'image-webpack-loader',
                    options: {
                        bypassOnDebug: true, // webpack@1.x
                        disable: true, // webpack@2.x and newer
                    },
                }],
                exclude: /node_modules/,
            }
        ]
    },
    devServer: {
        host: '0.0.0.0',
        port: 3000,
        contentBase: path.join(__dirname, 'dist'),
    },
    output: {
        path: __dirname + '/dist',
        filename: "bundle.js"
    },
    plugins: [
        new HtmlWebpackPlugin({
            hash: true,
            title: 'My App',
            template: 'src/index.html',
            filename: 'index.html',
            favicon: 'src/images/favicon.ico'
        })
    ],
    watchOptions: {
        ignored: /node_modules/
    }
};