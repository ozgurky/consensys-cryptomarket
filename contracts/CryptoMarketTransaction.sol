pragma solidity 0.4.24;

import "./CryptoMarketProduct.sol";
import "zeppelin/contracts/math/SafeMath.sol";


/**
 * @title CryptoMarketTransaction
 * @dev Provides users to purchase a product and also provides store owners withdraw their balances.
 */
contract CryptoMarketTransaction is CryptoMarketProduct {
    using SafeMath for uint256;

    event ProductSold(uint id);

    event BalanceWithdrawn(address owner, uint amount);

    /**
    * @dev Called by users to purchase selected product using their accounts when contract is not paused.
    * Triggers product sold event.
    */
    function buyProduct(uint _productId) public payable whenNotPaused {
        Product storage product = products[_productId];
        require(product.status && msg.value >= product.price && product.quantity > 0);
        uint extraValue = msg.value.sub(product.price);
        product.quantity = product.quantity.sub(1);
        uint currentBalance = balances[product.owner];
        balances[product.owner] = currentBalance.add(product.price);
        msg.sender.transfer(extraValue);

        uint transactionId = transactionIndex;
        transactionIndex = transactionIndex.add(1);
        transactions[transactionId] = Transaction({
            id : transactionId,
            productId : _productId,
            productName : product.name,
            buyer : msg.sender,
            seller : product.owner,
            price : product.price,
            quantity : 1,
            date : now
        });

        buyerTransactions[msg.sender].push(transactionId);
        sellerTransactions[product.owner].push(transactionId);

        emit ProductSold(_productId);
    }

    /**
    * @dev Returns products sold by sender.
    */
    function getPurchasedProductsAsSeller() public view returns (uint[]) {
        return sellerTransactions[msg.sender];
    }

    /**
    * @dev Returns products purchased by sender.
    */
    function getPurchasedProductsAsBuyer() public view returns (uint[]) {
        return buyerTransactions[msg.sender];
    }

    /**
    * @dev Returns balance of sender.
    */
    function getSenderBalance() public view returns (uint) {
        return balances[msg.sender];
    }

    /**
    * @dev Called by store owners when contract is not paused to withdraw their balance.
    * Triggers balance withdrawn event.
    */
    function withdraw() public onlyStoreOwners whenNotPaused {
        require(balances[msg.sender] > 0);
        uint amount = balances[msg.sender];
        balances[msg.sender] = 0;
        msg.sender.transfer(amount);
        emit BalanceWithdrawn(msg.sender, amount);
    }
}