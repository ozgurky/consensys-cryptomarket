pragma solidity 0.4.24;

import "./CryptoMarketTransaction.sol";
import "zeppelin/contracts/lifecycle/Destructible.sol";


/**
 * @title CryptoMarket
 * @dev Provides user role management of market.
 */
contract CryptoMarket is CryptoMarketTransaction, Destructible {
    /**
    * @dev Checks if the user (sender) has a super admin role.
    */
    function isSuperAdmin() public view returns(bool) {
        if (msg.sender == owner) {
            return true;
        }

        return false;
    }

    /**
    * @dev Checks if the user (sender) has an admin role.
    */
    function isAdmin() public view returns(bool) {
        return admins[msg.sender].status == true;
    }

    /**
    * @dev Checks if the user (sender) has a store owner role.
    */
    function isStoreOwner() public view returns(bool) {
        return storeOwners[msg.sender].status == true;
    }

    /**
    * @dev Returns sender roles as an array which includes if user is super admin, admin, store owner and customer.
    * @dev by default all addresses are users.
    */
    function getUserRolesArray() public view returns(uint[4]) {
        uint[4] memory rolesArray;
        if (msg.sender == owner) {
            rolesArray[0] = 1;
        }
        if (admins[msg.sender].status) {
            rolesArray[1] = 1;
        }
        if (storeOwners[msg.sender].status) {
            rolesArray[2] = 1;
        }

        rolesArray[3] = 1;
        return rolesArray;
    }
}