pragma solidity 0.4.24;

import "zeppelin/contracts/ownership/Ownable.sol";
import "zeppelin/contracts/lifecycle/Pausable.sol";


/**
 * @title CryptoMarketData
 * @dev Main contract which stores all CryptoMarket data structure.
 */
contract CryptoMarketData is Ownable, Pausable {
    struct Admin {
        uint id;
        string name;
        bool status;
    }

    mapping(address => Admin) public admins;
    address[] public adminIndex;

    struct StoreOwner {
        uint id;
        string name;
        bool status;
    }

    mapping(address => StoreOwner) public storeOwners;
    address[] public storeOwnerAddresses;

    uint internal storeFrontIndex = 1;

    struct StoreFront {
        uint id;
        address owner;
        string name;
        bool status;
    }

    mapping(address => uint[]) public storeFrontsToOwners;
    mapping(uint => StoreFront) public storeFronts;

    struct Product {
        uint id;
        address owner;
        string name;
        string description;
        uint price;
        uint quantity;
        bool status;
    }

    uint public productIndex = 1;
    mapping(uint => Product) public products;
    mapping(uint => uint[]) public productsToStoreFronts;

    mapping(address => uint) public balances;
    uint internal transactionIndex = 1;

    struct Transaction {
        uint id;
        uint productId;
        string productName;
        address buyer;
        address seller;
        uint price;
        uint quantity;
        uint date;
    }

    mapping(address => uint[]) public buyerTransactions;
    mapping(address => uint[]) public sellerTransactions;
    mapping(uint => Transaction) public transactions;
}