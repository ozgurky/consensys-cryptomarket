pragma solidity 0.4.24;

import "./CryptoMarketAdmin.sol";
import "zeppelin/contracts/math/SafeMath.sol";


/**
 * @title CryptoMarketStoreOwner
 * @dev Provides admins to create or delete store owners, receive store owner and count of store owners.
 */
contract CryptoMarketStoreOwner is CryptoMarketAdmin {
    using SafeMath for uint256;

    event StoreOwnerCreated(address storeOwnerAddress, string name, uint id);
    event StoreOwnerDeleted(address storeOwnerAddress, string name);

    /**
    * @dev Modifier to make a function callable only when the sender is an admin.
    */
    modifier onlyAdmins {
        require(admins[msg.sender].status == true);
        _;
    }

    /**
    * @dev Called by admins when contract is not paused  to create a store owner.
    * Triggers store owner created event.
    */
    function createStoreOwner(address _address, string _name) public onlyAdmins whenNotPaused {
        if (storeOwners[_address].status == true) {
            return;
        }

        uint storeOwnerId = storeOwnerAddresses.push(_address).sub(1);

        storeOwners[_address] = StoreOwner({
            id : storeOwnerId,
            name : _name,
            status : true
        });

        emit StoreOwnerCreated(_address, _name, storeOwnerId);
    }

    /**
    * @dev Called by admins when contract is not paused to delete a store owner.
    * Triggers store owner deleted event.
    */
    function deleteStoreOwner(address _address) public onlyAdmins whenNotPaused {
        uint rowToDelete = storeOwners[_address].id;
        address keyToMove = storeOwnerAddresses[storeOwnerAddresses.length.sub(1)];
        storeOwnerAddresses[rowToDelete] = keyToMove;
        storeOwners[_address].status = false;
        storeOwners[keyToMove].id = rowToDelete;
        storeOwnerAddresses.length.sub(1);
        storeOwnerAddresses.length = storeOwnerAddresses.length.sub(1);

        emit StoreOwnerDeleted(_address, storeOwners[_address].name);
    }

    /**
    * @dev Called by anyone and returns all store owner addresses.
    */
    function getStoreOwners() public view returns (address[]) {
        return storeOwnerAddresses;
    }

    /**
    * @dev Called by anyone and returns count of store owners.
    */
    function getStoreOwnersCount() public view returns (uint) {
        return storeOwnerAddresses.length;
    }
}