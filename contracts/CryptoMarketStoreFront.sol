pragma solidity 0.4.24;

import "./CryptoMarketStoreOwner.sol";
import "zeppelin/contracts/math/SafeMath.sol";


/**
 * @title CryptoMarketStoreFront
 * @dev Provides store owners to create store fronts and to add/remove/update products.
 */
contract CryptoMarketStoreFront is CryptoMarketStoreOwner {
    using SafeMath for uint256;

    event StoreFrontCreated(address storeOwnerAddress, string name, uint id);
    event StoreFrontUpdated(uint id, string name);

    /**
    * @dev Modifier to make a function callable only when the sender is a store owner.
    */
    modifier onlyStoreOwners {
        require(storeOwners[msg.sender].status == true);
        _;
    }

    /**
    * @dev Called by store owners when contract is not paused to create a store front.
    * Triggers store front created event.
    */
    function createStoreFront(string _name) public onlyStoreOwners whenNotPaused {
        uint id = storeFrontIndex;
        storeFrontIndex = storeFrontIndex.add(1);
        storeFronts[id] = StoreFront({
            id : id,
            owner : msg.sender,
            name : _name,
            status : true
        });

        storeFrontsToOwners[msg.sender].push(id);
        emit StoreFrontCreated(msg.sender, _name, id);
    }

    /**
    * @dev Called by store owners when contract is not paused to update a store front.
    * Triggers store front updated event.
    */
    function updateStoreFront(uint _id, string _name) public onlyStoreOwners whenNotPaused {
        require(storeFronts[_id].owner == msg.sender);
        storeFronts[_id].name = _name;
        emit StoreFrontUpdated(_id, _name);
    }

    /**
    * @dev Called by a store owner and returns her store fronts ids.
    */
    function getStoreFrontsByOwner() public view returns (uint[]) {
        return storeFrontsToOwners[msg.sender];
    }

    /**
    * @dev Returns all store front ids assigned to the given address.
    */
    function getStoreFrontsByAddress(address _address) public view returns (uint[]) {
        return storeFrontsToOwners[_address];
    }

}