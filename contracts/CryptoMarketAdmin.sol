pragma solidity 0.4.24;

import "./CryptoMarketData.sol";
import "zeppelin/contracts/math/SafeMath.sol";


/**
 * @title CryptoMarketAdmin
 * @dev  Admin role management contract which provides admin create/delete operations.
 * @dev There are 4 main role for this Crypto Market dapp: super admin, admin, store owner, user.
 * @dev Super admin (owner) is the creator of contract and can create/delete admins.
 * @dev Admins are responsible for creating/deleting store owners and can created only by super admin.
 * @dev Store owners are owner of the store fronts and can create, update delete store fronts and add/update/remove
 * products to these store fronts
 * @dev Users (customers) can display store fronts and their products which created by store owner and purchase products
 * @dev using MetaMask
 */
contract CryptoMarketAdmin is CryptoMarketData {
    using SafeMath for uint256;

    event AdminCreated(address adminAddress, string name, uint id);
    event AdminDeleted(address adminAddress, string name);

    /**
    * @dev Called by the super admin (owner) only to create a new admin with address and name.
    * Trigger an admin created event.
    */
    function createAdmin(address _address, string _name) public onlyOwner whenNotPaused {
        if (admins[_address].status == true) {
            return;
        }

        uint adminId = adminIndex.push(_address).sub(1);

        admins[_address] = Admin({
            id : adminId,
            name : _name,
            status : true
        });

        emit AdminCreated(_address, _name, adminId);
    }

    /**
    * @dev Called by the super admin (owner) only to delete an admin with her address only.
    * Trigger an admin deleted event.
    */
    function deleteAdmin(address _address) public onlyOwner whenNotPaused {
        uint rowToDelete = admins[_address].id;
        address keyToMove = adminIndex[adminIndex.length.sub(1)];
        adminIndex[rowToDelete] = keyToMove;
        admins[_address].status = false;
        admins[keyToMove].id = rowToDelete;
        adminIndex.length = adminIndex.length.sub(1);

        emit AdminDeleted(_address, admins[_address].name);
    }

    /**
    * @dev Called by anyone and returns all admin addresses.
    */
    function getAdmins() public view returns (address[]) {
        return adminIndex;
    }

    /**
    * @dev Called by anyone and returns admins count.
    */
    function getAdminsCount() public view returns (uint) {
        return adminIndex.length;
    }
}