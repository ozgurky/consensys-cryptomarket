pragma solidity 0.4.24;

import "./CryptoMarketStoreFront.sol";
import "zeppelin/contracts/math/SafeMath.sol";


/**
 * @title CryptoMarketProduct
 * @dev Provides store owners to manage their store front products.
 */
contract CryptoMarketProduct is CryptoMarketStoreFront {
    using SafeMath for uint256;

    event ProductCreated(
        uint id,
        address ownerAddress,
        uint storeFrontId,
        string name,
        uint price,
        uint quantity,
        bool status
    );

    event ProductUpdated(
        uint id,
        address ownerAddress,
        uint productId,
        string name,
        uint price,
        uint quantity,
        bool status
    );

    event ProductDeleted(uint productId);

    /**
    * @dev Called by store owners when contract is not paused to create a product.
    * Triggers product created event.
    */
    function createProduct
    (uint _storeFrontId, string _name, string _description, uint _price, uint _quantity, bool _status)
    public onlyStoreOwners whenNotPaused {
        require(storeFronts[_storeFrontId].owner == msg.sender);
        uint productId = productIndex;
        productIndex = productIndex.add(1);

        products[productId] = Product({
            id : productId,
            owner : msg.sender,
            name : _name,
            description : _description,
            price : _price,
            quantity : _quantity,
            status : _status
        });
        productsToStoreFronts[_storeFrontId].push(productId);

        emit ProductCreated(productId, msg.sender, _storeFrontId, _name, _price, _quantity, _status);
    }

    /**
    * @dev Called by store owners when contract is not paused to update a product.
    * Triggers product updated event.
    */
    function updateProduct
    (uint _productId, string _name, string _description, uint _price, uint _quantity, bool _status)
    public onlyStoreOwners whenNotPaused {
        require(products[_productId].owner == msg.sender);

        products[_productId].name = _name;
        products[_productId].description = _description;
        products[_productId].price = _price;
        products[_productId].quantity = _quantity;
        products[_productId].status = _status;


        emit ProductUpdated(_productId, msg.sender, _productId, _name, _price, _quantity, _status);
    }

    /**
    * @dev Called by store owners when contract is not paused to delete a product.
    * Triggers product deleted event.
    */
    function deleteProduct(uint _productId) public onlyStoreOwners whenNotPaused {
        require(products[_productId].owner == msg.sender);
        delete products[_productId];
        emit ProductDeleted(_productId);
    }

    /**
    * @dev Returns all product ids assigned to the given store front id.
    */
    function getProductsByStoreFrontId(uint _storeFrontId) public view returns (uint[]) {
        return productsToStoreFronts[_storeFrontId];
    }

}