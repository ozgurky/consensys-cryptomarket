# CryptoMarket

CryptoMarket is an Ethereum-Blockchain project that provides a market place where people can buy products using their Ethereum accounts.

You can see working version of this project at [https://ozgurkaya-crypto-market.herokuapp.com/#/](https://ozgurkaya-crypto-market.herokuapp.com/#/).

Project is also available on ipfs [https://gateway.ipfs.io/ipfs/QmNSt32SXNGuB76dziGRwFhC3BjGYEQPvvvgJNkQszFweY/](https://gateway.ipfs.io/ipfs/QmNSt32SXNGuB76dziGRwFhC3BjGYEQPvvvgJNkQszFweY/).

Basically an user visits the dapp and sees the store fronts which are created by store owners. By clicking a store front user reaches store front detail page. In this page all products with details is shown. Every product has a "buy" button in its card. Users clicks the button and Metamask opens a popup so transaction operation start.  

CryptoMarket has several account levels and role management. There are basically four role types:

- Super admin (owner)
- Admins
- Store Owner
- Users (customers) 

Every visitor of the app is an user by default. 


### Super Admin

The owner of contract will be the super admin of the market place. He/she can create and delete admins. Super admin can pause the matket and than reactivate it. Super admin also withdraw all money from contract in en emergency situtation.

### Admin

Admins can be created by _super admin_. They can create/delete store owners.

### Store Owners

Store owners are the sellers of the dapp. They can create store front for their products. After creating a store front store owner add products to the store front, update product's price, quantity etc. Store owners also can also remove the products from their store fronts. 

A store owner can create multiple store fronts.

Store owners also can withdraw all balance from their accounts on the dapp.

### Users

Everyone who has a etherum address is an user for CrytpoMarket. People can purchase listed products using their account on Metamask. 

## Technical Overview

CryptoMarket is a dapp project and designed to work on a ethereum blockchain platform.

The project is developed using truffle package. Main contracts is written on Solidity. Frontend parts of the projects are developed using React. 
Below the used packages are listed.

- [Truffle Framework](https://truffleframework.com/truffle) for writing, linking and testing contracts in a more organized pattern. 
- [Webpack](https://webpack.js.org/) for bundling javascript and creating a development server.
- [React](https://reactjs.org/) for building user interfaces and front end parts of the project.
- [Web3](https://github.com/ethereum/web3.js/) for interacting the dapp with blockchain.
- [Semantic UI React](http://react.semantic-ui.com/) for integrating ready html components into the project easily.

## System Requirements

- Nodejs version: v8.11.3
- Npm version: 6.1.0
- Ganache CLI version: v6.1.3
- Truffle version: v4.1.14

## Installation & Setup

If you have node & git installed it is enough creating a folder & copying all the code in this folder, installing dependencies and finally running the server. 

`mkdir crypto-market && cd crypto-market`
****
`git clone git@bitbucket.org:ozgurky/consensys-cryptomarket.git .`

`npm install --only=production`
(If you have any problem with npm please try to install dependencies with yarn `yarn install`)

`npm run start`

commands. After that you can visit the project at http://localhost:3000.

### Setup for development

Some of commands need to be run as root so please use `sudo` if you encounter any error.

`mkdir crypto-market && cd crypto-market`

CryptoMarket is a Truffle project so it is recommended to install truffle globally into your system.

- Install Truffle

`npm install -g truffle`

You need a local blockchain node, so you should install ganache or ganache-cli for interacting.

- Install Ganache

`npm install -g ganache-cli`

- Install node packages dependencies

`yarn install`

- Install Truffle dependencies

`truffle install`

- Start ganache-cli 

`ganache-cli` 

if you want your existing accounts and use the admin functions you should start ganache-cli with your mnenomic (recommended).

`ganache-cli -m "mnenomic words" -h "0.0.0.0"` 

- You can run smart contract tests

`truffle test`

- Compile contracts

`truffle compile`

- Clean deployed contracts & Migrate/deploy contracts

`rm -r build/contracts && truffle migrate`

- Run development server

`npm run dev`

- Visit the app

`http://localhost:3000`

You can also deploy the app on Rinkeby network adding an env file which includes your infura api key and mnenomic words. 
You can find these parameters in .env.example file. Basically copy .env.example as .env and change the parameter values. 
After that you should migrate contracts like following

`truffle migrate --network rinkeby --compile-all --reset`




 











