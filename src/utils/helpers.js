var Helper = {
    isSuperAdmin: function(roleArray) {
        return roleArray[0];
    },

    isAdmin: function(roleArray) {
        return roleArray[1];
    },

    isStoreOwner: function(roleArray) {
        return roleArray[2];
    },

    isUser: function(roleArray) {
        return roleArray[3];
    }
};

module.exports = Helper;