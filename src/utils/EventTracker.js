class EventTracker {
    events = [];
    start = true;

    addEvent(eventType, eventHash, from) {
        this.events.push({
            type: eventType,
            hash: eventHash
        });

        console.log("added---from::" + from, this.events);
    }

    checkEvent(eventType, eventHash) {
        let result = false;
        this.events.map(element => {
            console.log("element:::", element.hash , '===', eventHash);
            if (element.hash == eventHash) {
                result = true;
            }
        });

        if (!result) {
            this.addEvent(eventType, eventHash, 'inside');
        }

        return result;
    }
}

export default new EventTracker();