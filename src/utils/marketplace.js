import MarketPlaceContract from "../../build/contracts/CryptoMarket.json";
import contract from "truffle-contract";
import web3 from './web3';

const marketPlace = contract(MarketPlaceContract);
marketPlace.setProvider(web3.currentProvider);

if (typeof marketPlace.currentProvider.sendAsync !== "function") {
    marketPlace.currentProvider.sendAsync = function() {
        return marketPlace.currentProvider.send.apply(
            marketPlace.currentProvider, arguments
        );
    };
}

export default marketPlace;