import React, {Component} from 'react';
import {Dropdown, Menu} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import Helper from "../utils/helpers";

export default class MenuTop extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        //console.log("role", this.props.role);
        return (
            <Menu size='tiny'>
                <Menu.Item name='home' to='/' as={Link}/>

                <Menu.Menu position='right'>
                    {this.renderCustomerMenu()}
                    {this.renderStoreOwnerMenu()}
                    {this.renderAdminMenu()}
                    {this.renderSuperAdminMenu()}
                </Menu.Menu>
            </Menu>
        )
    }

    renderCustomerMenu() {
        if (!Helper.isUser(this.props.role)) {
            return null;
        }
        return (
            <Menu.Item name='My Orders' to={`/my-orders`} as={Link}/>
        );
    }

    renderStoreOwnerMenu() {
        if (!Helper.isStoreOwner(this.props.role)) {
            return null;
        }
        return (<Dropdown item text='Store Owner Menu'>
                    <Dropdown.Menu>
                        <Link to={`/withdraw`}>
                            <Dropdown.Item>Withdraw</Dropdown.Item>
                        </Link>
                        <Link to={`/my-sales`}>
                            <Dropdown.Item>My Sales</Dropdown.Item>
                        </Link>
                        <Link to={`/store-fronts/new`}>
                            <Dropdown.Item>New Store Front</Dropdown.Item>
                        </Link>
                        <Link to={`/store-fronts/list`}>
                            <Dropdown.Item>All Store Fronts</Dropdown.Item>
                        </Link>
                    </Dropdown.Menu>
             </Dropdown>
        );
    }

    renderAdminMenu() {
        if (!Helper.isAdmin(this.props.role)) {
            return null;
        }
        return (<Dropdown item text='Admin Menu'>
                <Dropdown.Menu>
                    <Link to={`/store-owners/new`}>
                        <Dropdown.Item>New Store Owner</Dropdown.Item>
                    </Link>
                    <Link to={`/store-owners/list`}>
                        <Dropdown.Item>All Store Owners</Dropdown.Item>
                    </Link>
                </Dropdown.Menu>
            </Dropdown>);
    }

    renderSuperAdminMenu() {
        if (!Helper.isSuperAdmin(this.props.role)) {
            return null;
        }
        return (<Dropdown item text='Super Admin Menu'>
            <Dropdown.Menu>
                <Link to={`/admins/new`}>
                    <Dropdown.Item>New Admin</Dropdown.Item>
                </Link>
                <Link to={`/admins/list`}>
                    <Dropdown.Item>All Admins</Dropdown.Item>
                </Link>
                <Link to={`/pause-market`}>
                    <Dropdown.Item>Pause Market</Dropdown.Item>
                </Link>
                <Link to={`/destroy-market`}>
                    <Dropdown.Item>Destroy Market</Dropdown.Item>
                </Link>
            </Dropdown.Menu>
        </Dropdown>);
    }
}