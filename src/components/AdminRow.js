import React, {Component} from 'react';
import {Table, Button, Confirm} from 'semantic-ui-react';
import marketPlace from '../utils/marketplace';
import web3 from '../utils/web3';

class AdminRow extends Component {
    state = {
        open: false,
        loading: false
    };

    onDeleteConfirm = () => this.setState({open: true});

    handleConfirm = async () => {
        this.setState({open: false, loading: true});
        const marketPlaceInstance = await marketPlace.deployed();
        const accounts = await web3.eth.getAccounts();
        this.props.error('');
        try {
            await marketPlaceInstance.deleteAdmin(this.props.address, {from: accounts[0]});
        } catch (err) {
            this.setState({loading:false});
            this.props.error(err.message);
            console.log(err.message);
        }
    };

    handleCancel = () => this.setState({open: false });

    render() {
        const {open, loading} = this.state;
        const {Row, Cell} = Table;
        return (
        <Row>
            <Cell>{this.props.id}</Cell>
            <Cell>{this.props.name}</Cell>
            <Cell>{this.props.address}</Cell>
            <Cell>
                <Confirm open={open} onCancel={this.handleCancel} onConfirm={this.handleConfirm} />
                <Button disabled={loading} loading={loading} negative onClick={this.onDeleteConfirm}>
                    Delete
                </Button>
            </Cell>
        </Row>
        );
    }
}

export default AdminRow;