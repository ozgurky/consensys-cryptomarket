import React, {Component} from 'react';
import {Table, Button, Confirm} from 'semantic-ui-react';
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";

export default class StoreOwnerProductRow extends Component {
    state = {
        open: false,
        loading: false
    };

    handleCancel = () => this.setState({open: false });

    onDeleteConfirm = () => this.setState({open: true});

    handleConfirm = async () => {
        this.setState({open: false, loading: true});
        const marketPlaceInstance = await marketPlace.deployed();
        const accounts = await web3.eth.getAccounts();
        try {
            await marketPlaceInstance.deleteProduct(this.props.id, {from: accounts[0]});
            this.setState({loading:false});
        } catch (err) {
            this.setState({loading:false});
            this.props.error(err.message);
            console.log(err.message);
        }
    };

    handleClick() {
        this.props.update(this.props);
    }

    render() {
        const {Row, Cell} = Table;
        const {open, loading} = this.state;
        return (
        <Row>
            <Cell>{this.props.id}</Cell>
            <Cell>{this.props.name}</Cell>
            <Cell>{this.props.quantity}</Cell>
            <Cell>{this.props.price}</Cell>
            <Cell>{this.props.status ? 'Yes' : 'No'}</Cell>
            <Cell textAlign="right">
                <Button color="blue" onClick={this.handleClick.bind(this)}>Update Product</Button>
                <Confirm open={open} onCancel={this.handleCancel} onConfirm={this.handleConfirm} />
                <Button disabled={loading} loading={loading} negative onClick={this.onDeleteConfirm}>
                    Remove Product
                </Button>
            </Cell>
        </Row>
        );
    }
};