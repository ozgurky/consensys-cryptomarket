import React, {Component} from 'react';
import {Table, Button} from 'semantic-ui-react';
import {Link} from "react-router-dom";

export default class StoreFrontRow extends Component {
    handleCancel = () => this.setState({open: false });

    render() {
        const {Row, Cell} = Table;
        return (
        <Row>
            <Cell>{this.props.id}</Cell>
            <Cell>{this.props.name}</Cell>
            <Cell textAlign="right">
                <Link to={'/store-fronts/' + this.props.id + '/update'}>
                    <Button color="blue">Update Store Front</Button>
                </Link>
                <Link to={'/store-fronts/' + this.props.id + '/products'}>
                    <Button>Manage Products</Button>
                </Link>
            </Cell>
        </Row>
        );
    }
};