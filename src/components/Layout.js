import React, {Component} from 'react';
import {Container, Icon, Message, Segment} from 'semantic-ui-react';
import MenuTop from "./Menu";
import AdminsList from "../pages/AdminsList";
import AdminForm from "../pages/AdminForm";
import StoreOwnerForm from "../pages/StoreOwnerForm";
import StoreOwnersList from "../pages/StoreOwnersList";
import StoreFrontForm from "../pages/StoreFrontForm";
import StoreFrontsList from "../pages/StoreFrontsList";
import StoreFrontUpdate from "../pages/StoreFrontUpdate";
import StoreFrontProducts from "../pages/StoreFrontProducts";
import Products from "../pages/Products";
import HomeIndex from "../pages/HomeIndex";
import MyOrders from "../pages/MyOrders";
import MySales from "../pages/MySales";
import Withdraw from "../pages/Withdraw";
import PauseMarket from "../pages/PauseMarket";
import DestroyMarket from "../pages/DestroyMarket";
import {Route} from "react-router-dom";
import web3 from "../utils/web3";

export default class Layout extends Component {
    state = {
        metaMaskLoaded: false
    }

    async componentDidMount() {
        const metaMaskLoaded = await web3.currentProvider.isMetaMask;
        this.setState({metaMaskLoaded: metaMaskLoaded})
    }

    renderMetaMaskNotLoaded() {
        return (
            <Container>
                <Message negative icon>
                    <Icon name='inbox' />
                    <Message.Content>
                        <Message.Header>MetaMask extension not installed</Message.Header>
                        You need to install MetaMask to use the web site. Please install it first. You can download the extension from <a href='https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn'>this link</a>.
                    </Message.Content>
                </Message>
            </Container>
        );
    }

    renderRoutes() {
        return (
            <div>
                <Route exact={true} path="/" render={(props) => (
                    <HomeIndex account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/pause-market" render={(props) => (
                    <PauseMarket account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/destroy-market" render={(props) => (
                    <DestroyMarket account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/my-orders" render={(props) => (
                    <MyOrders account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/withdraw" render={(props) => (
                    <Withdraw account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/my-sales" render={(props) => (
                    <MySales account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/products/:id" render={(props) => (
                    <Products account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/admins/list" render={(props) => (
                    <AdminsList account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/admins/new" render={(props) => (
                    <AdminForm account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/store-owners/list" render={(props) => (
                    <StoreOwnersList account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/store-owners/new" render={(props) => (
                    <StoreOwnerForm account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/store-fronts/list" render={(props) => (
                    <StoreFrontsList account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route exact={true} path="/store-fronts/new" render={(props) => (
                    <StoreFrontForm account={this.props.account} contract={this.props.contract} {...props}/>)}/>

                <Route path="/store-fronts/:id/update" render={(props) => (
                    <StoreFrontUpdate account={this.props.account} contract={this.props.contract} {...props}/>)}/>
                <Route path="/store-fronts/:id/products" render={(props) => (
                    <StoreFrontProducts account={this.props.account} contract={this.props.contract} {...props}/>)}/>
            </div>
        )
    }

    renderAddressBox() {
        const addressMessage = this.props.account ? this.props.account : 'Your address cannot be detected. Please check MetaMask extension';
        return (
            <Container style={{marginBottom:"10px"}}>
                <Segment>
                    Your address : {addressMessage}.
                </Segment>
            </Container>
        );
    }

    renderApp() {
        return (
            <div>
                {this.renderAddressBox()}
                {this.renderRoutes()}
            </div>
        );
    }

    render() {
        return (
            <Container>
                <div>
                    <MenuTop account={this.props.account} contract={this.props.contract} role={this.props.role}/>

                    {this.props.children}
                    {this.state.metaMaskLoaded ? this.renderApp() : this.renderMetaMaskNotLoaded()}
                </div>
            </Container>
        );
    }
};
