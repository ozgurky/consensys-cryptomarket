import React, { Component } from 'react';
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";
import {Table, Button, Form, Icon, Message, Segment, Input} from "semantic-ui-react";
import StoreOwnerProductRow from "../components/StoreOwnerProductRow";

export default class StoreFrontProducts extends Component {
    state = {
        productName: '',
        productDescription: '',
        productPrice: 0.0001,
        productQuantity: 0,
        productStatus: true,
        storeFrontName: '',
        errorMessage: '',
        loading: true,
        isOwner : 'pending',
        storeFrontId: null,
        products: [],
        formTitle: 'Create new product',
        updating: false,
        productId: null,
        blockNumber: 'latest',
        marketPlaceInstance: null
    };

    onUpdateSubmit = async (event) => {
        event.preventDefault();
        this.setState({loading: true, errorMessage: ''});
        try {
            if (this.state.productName == '') {
                throw "Please enter name!";
            }

            if (isNaN(this.state.productPrice) || this.state.productPrice <= 0) {
                throw "Please enter a valid price!";
            }

            if (isNaN(this.state.productQuantity) || this.state.productQuantity <= 0) {
                throw "Please enter a valid price!";
            }

            await this.props.contract.updateProduct(
                this.state.productId,
                this.state.productName,
                this.state.productDescription,
                web3.utils.toWei(this.state.productPrice, 'ether'),
                this.state.productQuantity,
                this.state.productStatus,
                {from: this.props.account}
            );

        } catch (err) {
            let errorMessage;
            errorMessage = err;

            if (typeof err.message != "undefined") {
                errorMessage = err.message;
            }

            this.setState({errorMessage: errorMessage, loading: false});
        }
    }

    onSubmit =  async (event) => {
        event.preventDefault();

        this.setState({loading: true, errorMessage: ''});

        try {
            if (this.state.productName == '') {
                throw "Please enter name!";
            }

            if (isNaN(this.state.productPrice) || this.state.productPrice <= 0) {
                throw "Please enter a valid price!";
            }

            if (isNaN(this.state.productQuantity) || this.state.productQuantity <= 0) {
                throw "Please enter a valid price!";
            }

            await this.props.contract.createProduct(
                this.props.match.params.id,
                this.state.productName,
                this.state.productDescription,
                web3.utils.toWei(this.state.productPrice, 'ether'),
                this.state.productQuantity,
                this.state.productStatus,
                {from: this.props.account}
            );

        } catch (err) {
            let errorMessage;
            errorMessage = err;

            if (typeof err.message != "undefined") {
                errorMessage = err.message;
            }

            this.setState({errorMessage: errorMessage, loading: false});
        }
    }

    async componentDidMount() {
        const marketPlaceInstance = await marketPlace.deployed();
        this.setState({
            storeFrontId: this.props.match.params.id,
            marketPlaceInstance: marketPlaceInstance
        });
        this.addEventListeners();
        const storeFront = await marketPlaceInstance.storeFronts(this.state.storeFrontId);
        const accounts = await web3.eth.getAccounts();
        if (web3.utils.toChecksumAddress(storeFront[1]) == web3.utils.toChecksumAddress(accounts[0])) {
            await this.getProducts();
            this.setState({
                storeFrontName: storeFront[2],
                loading: false,
                isOwner: 'yes'
            });
        } else {
            this.setState({
                loading: false,
                isOwner: 'no'
            });
        }
    }

    addEventListeners() {
        const {marketPlaceInstance} = this.state;
        const deleteEvent = marketPlaceInstance.ProductDeleted({}, {fromBlock: this.state.blockNumber});
        const self = this;
        deleteEvent.watch(async function(error, result){
            if (!error) {
                self.setState({blockNumber: result.blockNumber + 1});
                await self.getProducts();
            } else {
                console.log("event-error:::", error);
            }
        });


        const updateEvent = marketPlaceInstance.ProductUpdated({}, {fromBlock: this.state.blockNumber});
        updateEvent.watch(async function(error, result){
            if (!error) {
                self.setState({blockNumber: result.blockNumber + 1});
                await self.getProducts();
                self.setState({
                    errorMessage: '',
                    loading: false,
                    productId: null,
                    productName: '',
                    productDescription: '',
                    productPrice: 0,
                    productQuantity: 0,
                    productStatus: true,
                    updating: false,
                    formTitle: 'Create new product',
                });
            } else {
                console.log("event-error:::", error);
            }
        });

        const createEvent = marketPlaceInstance.ProductCreated({}, {fromBlock: this.state.blockNumber});
        createEvent.watch(async function(error, result){
            if (!error) {
                self.setState({blockNumber: result.blockNumber + 1});
                await self.getProducts();
                self.setState({
                    errorMessage: '',
                    loading: false,
                    productName: '',
                    productDescription: '',
                    productPrice: 0,
                    productQuantity: 0,
                    productStatus: true,
                });
            } else {
                console.log("event-error:::", error);
            }
        });
    }

    async getProducts() {
        const marketPlaceInstance = await marketPlace.deployed();
        const accounts = await web3.eth.getAccounts();
        const productKeys = await marketPlaceInstance.getProductsByStoreFrontId(this.state.storeFrontId, {from: accounts[0]});

        await Promise.all(
            productKeys
                .map((element, index) => {
                    return marketPlaceInstance.products(element);
                })
        ).then(result => {
            this.setState({products: result})
        });
    }

    render() {

        return (
            <Segment>
                <h3><b>{this.state.storeFrontName}</b> Products</h3>
                {this.state.isOwner == 'pending' ? this.renderLoading() : null}
                {this.state.isOwner == 'yes' ? this.renderList() : null}
                {this.state.isOwner == 'no' ? this.renderInvalidAccess() : null}
            </Segment>
        );
    }

    renderInvalidAccess() {
        return (
            <Message negative>
                <Message.Header>An Error Occurred!</Message.Header>
                <p>You are not the owner of this store front.</p>
            </Message>
        );
    }

    renderLoading() {
        return (
            <Message icon>
                <Icon name='circle notched' loading />
                <Message.Content>
                    <Message.Header>Just one second</Message.Header>
                    We are fetching that content for you.
                </Message.Content>
            </Message>
        );
    }

    renderForm() {
        return (
            <Segment>
                <h3>{this.state.formTitle}</h3>
                <Form onSubmit={this.state.updating ? this.onUpdateSubmit : this.onSubmit} error={!!this.state.errorMessage} loading={this.state.loading}>
                    <Form.Field>
                        <label>Product Name</label>
                        <input
                            required
                            placeholder='Product name'
                            value={this.state.productName}
                            onChange={event => this.setState({productName: event.target.value})}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Product Description</label>
                        <input
                            required
                            placeholder='Product description'
                            value={this.state.productDescription}
                            onChange={event => this.setState({productDescription: event.target.value})}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Product price</label>
                        <Input
                            type="number"
                            step="0.0001"
                            required
                            label="ether"
                            max="10"
                            min="0.0001"
                            labelPosition="right"
                            value={this.state.productPrice}
                            onChange={event => this.setState({productPrice: event.target.value})}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Product quantity</label>
                        <Input
                            type="number"
                            max="100"
                            min="1"
                            required
                            placeholder='Product quantity'
                            value={this.state.productQuantity}
                            onChange={event => this.setState({productQuantity: event.target.value})}
                        />
                    </Form.Field>
                    <Form.Checkbox
                        label='Product status'
                        checked={this.state.productStatus}
                        onClick={event => this.setState({productStatus: !this.state.productStatus})}
                    />
                    <Message error header="Oops!" content={this.state.errorMessage} />
                    <Button primary>{this.state.updating ? 'Update' : 'Create'}</Button>
                </Form>
            </Segment>
        );
    }

    renderUpdateForm(props) {
        this.setState({
            errorMessage: '',
            loading: false,
            productName: props.name,
            productDescription: props.description,
            productPrice: props.price,
            productQuantity: props.quantity,
            productStatus: props.status,
            formTitle: 'Update product',
            updating: true,
            productId: props.id
        });
    }

    renderRows() {
        return this.state.products
            .map((element, index) => {
                if (element[1] == '') {
                    return null;
                }
                return (
                    <StoreOwnerProductRow
                        key={index}
                        id={web3.utils.toDecimal(element[0])}
                        name={element[2]}
                        description={element[3]}
                        price={web3.utils.fromWei(element[4].toString(), 'ether')}
                        quantity={web3.utils.toDecimal(element[5])}
                        status={element[6]}
                        update={(props) => this.renderUpdateForm(props)}
                        error={(message) => {
                            this.setState({errorMessage: message});
                        }}
                    />
                );
            });
    }

    renderList() {
        const {Header, Row, HeaderCell, Body} = Table;
        return (
            <div>
                <Table>
                    <Header>
                        <Row>
                            <HeaderCell>ID</HeaderCell>
                            <HeaderCell>Name</HeaderCell>
                            <HeaderCell>Quantity</HeaderCell>
                            <HeaderCell>Price (Ether)</HeaderCell>
                            <HeaderCell>Active</HeaderCell>
                            <HeaderCell></HeaderCell>
                        </Row>
                    </Header>
                    <Body>
                    {this.renderRows()}
                    </Body>
                </Table>
                <div>Found {this.state.products.length} products.</div>
                {this.renderForm()}
            </div>
        );
    }
};