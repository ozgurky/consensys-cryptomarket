import React, { Component } from 'react';
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";
import {Button, Card, Container, Header, Segment, Image, Modal, Icon, Message} from "semantic-ui-react";
import noImage from "../images/no-image-icon.png";
import eventTracker from "../utils/EventTracker";

export default class Products extends Component {
    state = {
        storeFrontId: null,
        storeFront : [],
        products: [],
        marketPlaceInstance: null,
        accounts: null,
        loading: false,
        buyLoading: 0,
        open: false,
        result: false,
        resultMessage: '',
        blockNumber: 'latest'
    };

    async handleBuy(product) {
        const {marketPlaceInstance, accounts} = this.state;
        this.setState({
            buyLoading: product[0].toNumber(),
        });
        try {
            await marketPlaceInstance.buyProduct(product[0].toNumber(), {from: accounts[0], value: product[4].toNumber()});
        } catch (error) {
            this.setState({
                buyLoading: 0,
                open: true,
                result: false,
                resultMessage: error.message
            });
        }
    }

    async componentDidMount() {
        this.setState({
            storeFrontId: this.props.match.params.id,
            marketPlaceInstance: await marketPlace.deployed(),
            accounts: await web3.eth.getAccounts(),
        });

        await this.addEventListeners();
        await this.getStoreFront();
        await this.getProducts();
    }

    async addEventListeners() {
        const {marketPlaceInstance, blockNumber} = this.state;

        const soldEvent = marketPlaceInstance.ProductSold({}, {fromBlock: blockNumber, remove:true});
        const self = this;
        await soldEvent.watch(async function(error, result){
            if (!error && !eventTracker.checkEvent(result.event, result.transactionHash)) {
                await self.getProducts();
                self.setState({
                    buyLoading: 0,
                    open: true,
                    result: true,
                    resultMessage: 'Your transaction has completed',
                });
            } else {
                console.log("event-error:::", error);
            }
        });
    }

    async getStoreFront() {
        const storeFront = await this.state.marketPlaceInstance.storeFronts(this.state.storeFrontId);
        this.setState({storeFront: storeFront});
    }

    async getProducts() {
        const {storeFrontId, marketPlaceInstance} = this.state;
        const storeFrontProducts = await marketPlaceInstance.getProductsByStoreFrontId(storeFrontId);
        let products = storeFrontProducts.map(product => product.valueOf());

        await Promise.all(
            products
                .map(async productId => {
                    return await marketPlaceInstance.products(productId);
                })
        ).then(result => {
           this.setState({products: result, loading: false});
        });
    }

    close = () => this.setState({ open: false });
    open = () => this.setState({ open: true });

    renderModal() {
        const {open, result, resultMessage} = this.state;
        return (
            <div>
                <Modal
                    open={open}
                    closeOnEscape={true}
                    closeOnDimmerClick={false}
                    onClose={this.close}
                >
                    <Header icon='ethereum' content='Purchase action result' />
                    <Modal.Content>
                        <Message positive={result} negative={!result}>
                            <Message.Header>{resultMessage}</Message.Header>
                            <p>
                                Go to your <b>products</b> page to see details.
                            </p>
                        </Message>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button
                            onClick={this.close}
                            positive
                            labelPosition='right'
                            icon='checkmark'
                            content='Ok'
                        />
                    </Modal.Actions>
                </Modal>
            </div>
        );
    }

    renderProducts() {
        return this.state.products
            .map((product, index) => {
                return (
                    <Card key={index}>
                        <Image src={noImage} />
                        <Card.Content>
                            <Card.Header>{product[2]}</Card.Header>
                            <Card.Meta style={{fontWeight:'bold'}}>{web3.utils.fromWei(product[4].valueOf(), 'ether')} ether</Card.Meta>
                            <Card.Description>{product[3]}</Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                            <a>
                                {product[4].valueOf() > 0 ? product[5].valueOf() + ' items remaining' : 'No stock available'}
                            </a>
                            <Container textAlign='right'>
                                <Button disabled={this.state.buyLoading != 0 || product[5].valueOf() == 0} loading={this.state.buyLoading == product[0].toNumber()} size='small' color='green' data-id={product[0].valueOf()} onClick={this.handleBuy.bind(this, product)}>
                                    Buy
                                </Button>
                            </Container>
                        </Card.Content>
                    </Card>
                );
        });

    }

    render() {
        return (
            <div>
                <Segment loading={this.state.loading}>
                    <Header as='h2'>{this.state.storeFront[2]} products</Header>
                    <Card.Group>
                        {this.renderProducts()}
                    </Card.Group>
                    {this.renderModal()}
                </Segment>
            </div>
        );
    }
};