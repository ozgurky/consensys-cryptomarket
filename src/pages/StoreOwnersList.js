import React, { Component } from 'react';
import {Form, Message, Table, Segment} from 'semantic-ui-react';
import web3 from '../utils/web3';
import marketPlace from '../utils/marketplace';
import StoreOwnerRow from "../components/StoreOwnerRow";

export default class StoreOwnersList extends Component {
    state = {
        storeOwnersCount: 0,
        storeOwners: [],
        storeOwnersAddresses: [],
        marketPlaceInstance: null,
        errorMessage: ''
    };

    async componentDidMount() {
        const marketPlaceInstance = await marketPlace.deployed();
        await this.getStoreOwners();

        let blockNumber = 'latest';
        const deleteEvent = marketPlaceInstance.StoreOwnerDeleted({}, {fromBlock: blockNumber});
        const createEvent = marketPlaceInstance.StoreOwnerCreated({}, {fromBlock: blockNumber});
        const self = this;
        createEvent.watch(function(error, result){
            if (!error) {
                blockNumber = result.blockNumber + 1;
                console.log("**store owner event-created:::", result);
                self.getStoreOwners();
            } else {
                console.log("event-error:::", error);
            }
        });
        deleteEvent.watch(function(error, result){
            if (!error) {
                blockNumber = result.blockNumber + 1;
                console.log("**store owner event-deleted:::", result);
                self.getStoreOwners();
            } else {
                console.log("event-error:::", error);
            }
        });
    }

    async getStoreOwners() {
        const marketPlaceInstance = await marketPlace.deployed();
        const storeOwnersCount = await marketPlaceInstance.getStoreOwnersCount();
        const storeOwnersAddresses = await marketPlaceInstance.getStoreOwners();

        await Promise.all(
            storeOwnersAddresses
                .map((element, index) => {
                    return marketPlaceInstance.storeOwners(element);
                })
        ).then(result => this.setState({storeOwners: result}));

        this.setState({
            storeOwnersCount: web3.utils.toDecimal(storeOwnersCount),
            storeOwnersAddresses: storeOwnersAddresses,
            marketPlaceInstance: marketPlaceInstance
        });
    }

    renderRows() {
        return this.state.storeOwners
            .map((element, index) => {
                return (
                    <StoreOwnerRow
                        key={index}
                        address={this.state.storeOwnersAddresses[index]}
                        id={web3.utils.toDecimal(element[0])}
                        name={element[1]}
                        error={(message) => {
                            this.setState({errorMessage: message});
                        }}
                    />
                );
            });
    }

    render() {
        const {Header, Row, HeaderCell, Body} = Table;

        return (
            <Form error={!!this.state.errorMessage}>
                <Segment>
                    <h3>All store owners</h3>
                    <Table>
                        <Header>
                            <Row>
                                <HeaderCell>ID</HeaderCell>
                                <HeaderCell>Name</HeaderCell>
                                <HeaderCell>Address</HeaderCell>
                                <HeaderCell></HeaderCell>
                            </Row>
                        </Header>
                        <Body>
                        {this.renderRows()}
                        </Body>
                    </Table>
                    <div>Found {this.state.storeOwnersCount} store owners.</div>
                    <Message error header="Oops!" content={this.state.errorMessage} />
                </Segment>
            </Form>
        );
    }
}