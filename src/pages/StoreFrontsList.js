import React, { Component } from 'react';
import {Form, Message, Table, Segment} from "semantic-ui-react";
import StoreFrontRow from "../components/StoreFrontRow";
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";

export default class StoreFrontsList extends Component {
    state = {
        storeFrontsCount: 0,
        storeFronts: [],
        marketPlaceInstance: null,
        errorMessage: ''
    };

    async componentDidMount() {
        const marketPlaceInstance = await marketPlace.deployed();
        this.getStoreFronts();

        const createEvent = marketPlaceInstance.StoreFrontCreated({}, {fromBlock: 'latest'});
        const self = this;
        createEvent.watch(function(error, result){
            if (!error) {
                console.log("**event-created-store-front:::", result);
                self.getStoreFronts();
            } else {
                console.log("event-error:::", error);
            }
        });

        const updateEvent = marketPlaceInstance.StoreFrontUpdated({}, {fromBlock: 'latest'});
        updateEvent.watch(function(error, result){
            if (!error) {
                self.getStoreFronts();
            } else {
                console.log("event-error:::", error);
            }
        });
    }

    async getStoreFronts() {
        const marketPlaceInstance = await marketPlace.deployed();
        const accounts = await web3.eth.getAccounts();
        const storeFrontKeys = await marketPlaceInstance.getStoreFrontsByOwner({from: accounts[0]});

        await Promise.all(
            storeFrontKeys
                .map((element, index) => {
                    return marketPlaceInstance.storeFronts(element);
                })
        ).then(result => {
            this.setState({storeFronts: result.filter(element => element != null)})
        });


        this.setState({
            storeFrontsCount: storeFrontKeys.length,
            marketPlaceInstance: marketPlaceInstance
        });

    }

    renderRows() {
        return this.state.storeFronts
            .map((element, index) => {
                return (
                    <StoreFrontRow
                        key={index}
                        id={web3.utils.toDecimal(element[0])}
                        name={element[2]}
                        error={(message) => {
                            this.setState({errorMessage: message});
                        }}
                    />
                );
            });
    }

    render() {
        const {Header, Row, HeaderCell, Body} = Table;

        return (
            <Form error={!!this.state.errorMessage}>
                <Segment>
                    <h3>My all store fronts</h3>
                    <Table>
                        <Header>
                            <Row>
                                <HeaderCell>ID</HeaderCell>
                                <HeaderCell>Name</HeaderCell>
                                <HeaderCell></HeaderCell>
                            </Row>
                        </Header>
                        <Body>
                        {this.renderRows()}
                        </Body>
                    </Table>
                    <div>Found {this.state.storeFrontsCount} store fronts.</div>
                    <Message error header="Oops!" content={this.state.errorMessage} />
                </Segment>
            </Form>
        );
    }
};