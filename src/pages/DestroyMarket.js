import React, { Component } from 'react';
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";
import {Header, Button, Container, Icon, Message, Segment, Confirm} from "semantic-ui-react";
import eventTracker from "../utils/EventTracker";

export default class DestroyMarket extends Component {
    state = {
        marketPlaceInstance: null,
        accounts: null,
        loading: true,
        isSuperAdmin: 'pending',
        currentBalance: 0,
        open: false,
        buttonLoading: false,
        errorMessage: '',
        successMessage: '',
        blockNumber: 'latest'
    };

    async componentDidMount() {
        const marketPlaceInstance = await marketPlace.deployed();
        const accounts = await web3.eth.getAccounts();
        this.setState({
            marketPlaceInstance: marketPlaceInstance,
            accounts: accounts,
        });

        const superAdmin = await marketPlaceInstance.isSuperAdmin({from: accounts[0]});
        if (superAdmin) {
            this.setState({
                isSuperAdmin: 'yes',
                loading: false,
                currentBalance: await web3.eth.getBalance(marketPlaceInstance.address)
            });
        } else {
            this.setState({isSuperAdmin: 'no', loading: false});
        }
    }

    handleConfirm = async () => {
        const {marketPlaceInstance, accounts} = this.state;
        this.setState({
            open: false,
            buttonLoading: true
        });
        try {
            await marketPlaceInstance.destroy({from: accounts[0]});
        } catch (error) {
            console.log(error);
            this.setState({
                buttonLoading: false,
                errorMessage: error.message
            });
        }
    };
    handleCancel = () => this.setState({open: false });

    onTerminateConfirm = () => this.setState({open: true, errorMessage: '', successMessage: ''});

    renderTerminateForm() {
        const {currentBalance, open, buttonLoading, errorMessage, successMessage} = this.state;

        return (
            <div>
                <span>You can terminate & withdraw all market balance using the button below</span>
                <p>
                    <Icon name="ethereum"></Icon>
                    <span>Crypto Market Balance : {web3.utils.fromWei(currentBalance, 'ether')} ether</span>
                </p>
                <Container textAlign='center'>
                    <Confirm open={open} onCancel={this.handleCancel} onConfirm={this.handleConfirm} />
                    <Button size='huge' loading={buttonLoading} primary disabled={buttonLoading} onClick={this.onTerminateConfirm}>
                        Terminate Market
                    </Button>
                </Container>
                <Message error hidden={!errorMessage} header="Oops!" content={errorMessage} />
                <Message success hidden={!successMessage} header="" content={successMessage} />
            </div>
        );
    }

    renderInvalidAccess() {
        return (
            <Message negative>
                <Message.Header>An Error Occurred!</Message.Header>
                <p>You cannot access this page, permission denied.</p>
            </Message>
        );
    }

    render() {
        return (
            <div>
                <Segment loading={this.state.loading}>
                    <Header as='h3'>Terminate & Withdraw Market</Header>
                    {this.state.isSuperAdmin == 'yes' ? this.renderTerminateForm() : null}
                    {this.state.isSuperAdmin == 'no'? this.renderInvalidAccess() : null}
                </Segment>
            </div>
        );
    };
};