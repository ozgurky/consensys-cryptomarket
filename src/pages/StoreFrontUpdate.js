import React, { Component } from 'react';
import {Button, Form, Message, Icon, Segment} from "semantic-ui-react";
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";
import eventTracker from "../utils/EventTracker";

export default class StoreFrontUpdate extends Component {
    state = {
        name: '',
        errorMessage: '',
        loading: true,
        isOwner : 'pending',
    };

    onSubmit =  async (event) => {
        event.preventDefault();

        this.setState({loading: true, errorMessage: ''});

        try {
            if (this.state.name == '') {
                throw "Please enter name!";
            }

            await this.props.contract.updateStoreFront(
                this.props.match.params.id,
                this.state.name,
                {from: this.props.account}
            );

        } catch (err) {
            let errorMessage;
            errorMessage = err;

            if (typeof err.message != "undefined") {
                errorMessage = err.message;
            }

            this.setState({errorMessage: errorMessage, loading: false});
        }
    };

    async componentDidMount() {
        const storeFrontId = this.props.match.params.id;
        const marketPlaceInstance = await marketPlace.deployed();
        const storeFront = await marketPlaceInstance.storeFronts(storeFrontId);
        const accounts = await web3.eth.getAccounts();
        if (web3.utils.toChecksumAddress(storeFront[1]) == web3.utils.toChecksumAddress(accounts[0])) {
            this.setState({
                name: storeFront[2],
                loading: false,
                isOwner: 'yes'
            });
        } else {
            this.setState({
                loading: false,
                isOwner: 'no'
            });
        }

        const updateEvent = marketPlaceInstance.StoreFrontUpdated({}, {fromBlock: 'latest'});
        self = this;
        updateEvent.watch(function(error, result){
            if (!error  && !eventTracker.checkEvent(result.event, result.transactionHash)) {
                console.log("stop");
                self.props.history.push('/store-fronts/list');
            } else {
                console.log("event-error:::", error);
            }
        });
    }

    render() {

        return (
            <Segment>
                <h3>Update store front!</h3>
                {this.state.isOwner == 'pending' ? this.renderLoading() : null}
                {this.state.isOwner == 'yes' ? this.renderForm() : null}
                {this.state.isOwner == 'no' ? this.renderInvalidAccess() : null}
            </Segment>
        );
    }

    renderInvalidAccess() {
        return (
            <Message negative>
                <Message.Header>An Error Occurred!</Message.Header>
                <p>You are not the owner of this store front.</p>
            </Message>
        );
    }

    renderLoading() {
        return (
            <Message icon>
                <Icon name='circle notched' loading />
                <Message.Content>
                    <Message.Header>Just one second</Message.Header>
                    We are fetching that content for you.
                </Message.Content>
            </Message>
        );
    }

    renderForm() {
        return (
            <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage} loading={this.state.loading}>
                <Form.Field>
                    <label>Store Front Name</label>
                    <input
                        placeholder='Store front name'
                        value={this.state.name}
                        onChange={event => this.setState({name: event.target.value})}
                    />
                </Form.Field>
                <Message error header="Oops!" content={this.state.errorMessage} />
                <Button primary>Update</Button>
            </Form>
        );
    }
};