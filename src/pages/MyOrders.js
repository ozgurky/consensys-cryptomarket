import React, { Component } from 'react';
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";
import {Header, Segment, Message, Item} from "semantic-ui-react";
import noImage from "../images/no-image-icon.png";

export default class MyOrders extends Component {
    state = {
        marketPlaceInstance: null,
        accounts: null,
        transactions: [],
        loading: true
    };

    async componentDidMount() {
        this.setState({
            marketPlaceInstance: await marketPlace.deployed(),
            accounts: await web3.eth.getAccounts(),
        });

        await this.getTransactions();
    }

    async getTransactions() {
        const {accounts, marketPlaceInstance} = this.state;
        const transactions = await marketPlaceInstance.getPurchasedProductsAsBuyer({from: accounts[0]});
        await Promise.all(
            transactions
                .map(async transaction => {
                    return await marketPlaceInstance.transactions(transaction.valueOf());
                })
        ).then(result => {
            this.setState({transactions: result.reverse(), loading: false});
        });
    }

    renderTransactions() {
        if (this.state.transactions.length == 0) {
            return (
                <Message>No transactions found</Message>
            );
        }
        return this.state.transactions.map((transaction, index) => {
            const transactionDate = new Date(transaction[7].valueOf() * 1000);
            return (
                <Item key={index}>
                    <Item.Image size='tiny' src={noImage} />

                    <Item.Content>
                        <Item.Header as='a'>{transaction[2]}</Item.Header>
                        <Item.Meta>{transactionDate.toUTCString()}</Item.Meta>
                        <Item.Description>
                            <p><span>Price : {web3.utils.fromWei(transaction[5].valueOf(), 'ether')} ether</span></p>
                            <p><span>Quantity : {transaction[6].valueOf()}</span></p>
                        </Item.Description>
                        <Item.Extra>Seller : {transaction[4].valueOf()}</Item.Extra>
                    </Item.Content>
                </Item>
            );
        });
    }

    render() {
        return (
            <div>
                <Segment loading={this.state.loading}>
                    <Header as='h2'>My Orders</Header>
                    <Item.Group divided>
                        {this.renderTransactions()}
                    </Item.Group>
                </Segment>
            </div>
        );
    };
};