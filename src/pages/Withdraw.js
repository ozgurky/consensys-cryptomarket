import React, { Component } from 'react';
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";
import {Header, Button, Container, Icon, Message, Segment, Confirm} from "semantic-ui-react";
import eventTracker from "../utils/EventTracker";

export default class Withdraw extends Component {
    state = {
        marketPlaceInstance: null,
        accounts: null,
        transactions: [],
        loading: true,
        isStoreOwner: 'pending',
        currentBalance: 0,
        open: false,
        buttonLoading: false,
        errorMessage: '',
        successMessage: '',
        blockNumber: 'latest'
    };

    async componentDidMount() {
        const marketPlaceInstance = await marketPlace.deployed();
        const accounts = await web3.eth.getAccounts();
        this.setState({
            marketPlaceInstance: marketPlaceInstance,
            accounts: accounts,
        });

        await this.addEventListeners();

        const storeOwner = await marketPlaceInstance.storeOwners(accounts[0]);
        if (storeOwner[2]) {
            const balance = await marketPlaceInstance.getSenderBalance({from: accounts[0]});
            this.setState({
                isStoreOwner: 'yes',
                loading: false,
                currentBalance: balance.valueOf()
            });
        } else {
            this.setState({isStoreOwner: 'no', loading: false});
        }
    }

    async addEventListeners() {
        const {marketPlaceInstance, blockNumber, accounts} = this.state;

        const withdrawEvent = marketPlaceInstance.BalanceWithdrawn({}, {fromBlock: blockNumber});
        const self = this;
        withdrawEvent.watch(async function(error, result){
            if (!error && !eventTracker.checkEvent(result.event, result.transactionHash)) {
                const balance = await marketPlaceInstance.getSenderBalance({from: accounts[0]});
                self.setState({
                    buttonLoading: false,
                    currentBalance: balance.valueOf(),
                    successMessage: 'Your withdraw operation completed',
                });
            } else {
                console.log("event-error:::", error);
            }
        });
    }

    handleConfirm = async () => {
        const {marketPlaceInstance, accounts} = this.state;
        this.setState({
            open: false,
            buttonLoading: true
        });
        try {
            await marketPlaceInstance.withdraw({from: accounts[0], gas: 1000000});
        } catch (error) {
            console.log(error);
            this.setState({
                buttonLoading: false,
                errorMessage: error.message
            });
        }
    };
    handleCancel = () => this.setState({open: false });

    onWithdrawConfirm = () => this.setState({open: true, errorMessage: '', successMessage: ''});

    renderWithdrawForm() {
        const {currentBalance, open, buttonLoading, errorMessage, successMessage} = this.state;

        return (
            <div>
                <Icon name="ethereum"></Icon>
                <span>Your Balance : {web3.utils.fromWei(currentBalance, 'ether')} ether</span>
                <Container textAlign='center'>
                    <Confirm open={open} onCancel={this.handleCancel} onConfirm={this.handleConfirm} />
                    <Button size='huge' loading={buttonLoading} primary disabled={currentBalance <= 0 || buttonLoading} onClick={this.onWithdrawConfirm}>Withdraw My Balance</Button>
                </Container>
                <Message error hidden={!errorMessage} header="Oops!" content={errorMessage} />
                <Message success hidden={!successMessage} header="" content={successMessage} />
            </div>
        );
    }

    renderInvalidAccess() {
        return (
            <Message negative>
                <Message.Header>An Error Occurred!</Message.Header>
                <p>You cannot access this page, permission denied.</p>
            </Message>
        );
    }

    render() {
        return (
            <div>
                <Segment loading={this.state.loading}>
                    <Header as='h3'>Balance Details</Header>
                    {this.state.isStoreOwner == 'yes' ? this.renderWithdrawForm() : null}
                    {this.state.isStoreOwner == 'no'? this.renderInvalidAccess() : null}
                </Segment>
            </div>
        );
    };
};