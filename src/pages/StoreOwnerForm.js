import React, { Component } from 'react';
import {Form, Button, Message, Segment} from 'semantic-ui-react';
import web3 from '../utils/web3.js';
import eventTracker from "../utils/EventTracker";

export default class StoreOwnerForm extends Component {
    state = {
        name: '',
        address: '',
        errorMessage: '',
        loading: false
    };

    onSubmit =  async (event) => {
        event.preventDefault();

        this.setState({loading: true, errorMessage: ''});

        try {
            if (this.state.name == '') {
                throw "Please enter name!";
            }

            if (!web3.utils.isAddress(this.state.address)) {
                throw("Please enter a valid address!");
            }

            await this.props.contract.createStoreOwner(
                this.state.address,
                this.state.name,
                {from: this.props.account}
            );

            const createEvent = this.props.contract.StoreOwnerCreated({}, {fromBlock: 'latest'});
            const self = this;
            createEvent.watch(function (error, result) {
                if (!error && !eventTracker.checkEvent(result.event, result.transactionHash)) {
                    self.props.history.push('/store-owners/list');
                } else {
                    console.log("event-error:::", error);
                }
            });
        } catch (err) {
            let errorMessage;
            errorMessage = err;

            if (typeof err.message != "undefined") {
                errorMessage = err.message;
            }

            this.setState({errorMessage: errorMessage, loading: false});
        }
    };

    render() {
        return (
            <Segment>
                <h3>Create a store owner!</h3>
                <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage} loading={this.state.loading}>
                    <Form.Field>
                        <label>Store Owner Name</label>
                        <input
                            placeholder='Store owner name'
                            value={this.state.name}
                            onChange={event => this.setState({name: event.target.value})}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Store Owner Address</label>
                        <input
                            placeholder='Store owner address'
                            value={this.state.address}
                            onChange={event => this.setState({address: event.target.value})}
                        />
                    </Form.Field>
                    <Message error header="Oops!" content={this.state.errorMessage} />
                    <Button primary>Create</Button>
                </Form>
            </Segment>
        );
    }
}
