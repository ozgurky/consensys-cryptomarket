import React, { Component } from 'react';
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";
import {Header, Button, Container, Message, Segment, Confirm} from "semantic-ui-react";
import eventTracker from "../utils/EventTracker";

export default class PauseMarket extends Component {
    state = {
        marketPlaceInstance: null,
        accounts: null,
        loading: true,
        isSuperAdmin: 'pending',
        paused: false,
        open: false,
        buttonLoading: false,
        errorMessage: '',
        successMessage: '',
        blockNumber: 'latest'
    };

    async componentDidMount() {
        const marketPlaceInstance = await marketPlace.deployed();
        const accounts = await web3.eth.getAccounts();
        this.setState({
            marketPlaceInstance: marketPlaceInstance,
            accounts: accounts,
        });

        await this.addEventListeners();

        const superAdmin = await marketPlaceInstance.isSuperAdmin({from: accounts[0]});
        if (superAdmin) {
            const status = await marketPlaceInstance.paused({from: accounts[0]});
            this.setState({
                isSuperAdmin: 'yes',
                loading: false,
                paused: status
            });
        } else {
            this.setState({isSuperAdmin: 'no', loading: false});
        }
    }

    async addEventListeners() {
        const {marketPlaceInstance, blockNumber, accounts} = this.state;

        const pauseEvent = marketPlaceInstance.Pause({}, {fromBlock: blockNumber});
        const unPauseEvent = marketPlaceInstance.Unpause({}, {fromBlock: blockNumber});
        const self = this;
        pauseEvent.watch(async function(error, result){
            if (!error && !eventTracker.checkEvent(result.event, result.transactionHash)) {
                const status = await marketPlaceInstance.paused({from: accounts[0]});
                self.setState({
                    buttonLoading: false,
                    paused: status,
                    successMessage: 'Crypto Market paused',
                });
            } else {
                console.log("event-error:::", error);
            }
        });

        unPauseEvent.watch(async function(error, result){
            if (!error && !eventTracker.checkEvent(result.event, result.transactionHash)) {
                const status = await marketPlaceInstance.paused({from: accounts[0]});
                self.setState({
                    buttonLoading: false,
                    paused: status,
                    successMessage: 'Crypto Market activated',
                });
            } else {
                console.log("event-error:::", error);
            }
        });
    }

    handleConfirm = async () => {
        const {marketPlaceInstance, accounts} = this.state;
        this.setState({
            open: false,
            buttonLoading: true
        });
        try {
            if (this.state.paused) {
                await marketPlaceInstance.unpause({from: accounts[0]});
            } else {
                await marketPlaceInstance.pause({from: accounts[0]});
            }

        } catch (error) {
            console.log(error);
            this.setState({
                buttonLoading: false,
                errorMessage: error.message
            });
        }
    };
    handleCancel = () => this.setState({open: false });

    onPauseConfirm = () => this.setState({open: true, errorMessage: '', successMessage: ''});

    renderPauseForm() {
        const {paused, open, buttonLoading, errorMessage, successMessage} = this.state;

        return (
            <div>
                <span>You can pause/unpause Crypto Market using the button below</span>
                <p>Current Market Status : <b>{paused ? 'Paused' : 'Active'}</b></p>
                <Container textAlign='center'>
                    <Confirm open={open} onCancel={this.handleCancel} onConfirm={this.handleConfirm} />
                    <Button size='huge' loading={buttonLoading} primary disabled={buttonLoading} onClick={this.onPauseConfirm}>
                        {paused ? 'Unpause' : 'Pause'} Market
                    </Button>
                </Container>
                <Message error hidden={!errorMessage} header="Oops!" content={errorMessage} />
                <Message success hidden={!successMessage} header="" content={successMessage} />
            </div>
        );
    }

    renderInvalidAccess() {
        return (
            <Message negative>
                <Message.Header>An Error Occurred!</Message.Header>
                <p>You cannot access this page, permission denied.</p>
            </Message>
        );
    }

    render() {
        return (
            <div>
                <Segment loading={this.state.loading}>
                    <Header as='h3'>Pause & Unpause Market</Header>
                    {this.state.isSuperAdmin == 'yes' ? this.renderPauseForm() : null}
                    {this.state.isSuperAdmin == 'no'? this.renderInvalidAccess() : null}
                </Segment>
            </div>
        );
    };
};