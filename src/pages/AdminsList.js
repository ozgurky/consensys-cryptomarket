import React, { Component } from 'react';
import {Form, Message, Table, Segment} from 'semantic-ui-react';
import web3 from '../utils/web3';
import marketPlace from '../utils/marketplace';
import AdminRow from "../components/AdminRow";

export default class AdminsList extends Component {
    state = {
        adminCount: 0,
        admins: [],
        adminsAddresses: [],
        marketPlaceInstance: null,
        errorMessage: ''
    };

    async componentDidMount() {
        const marketPlaceInstance = await marketPlace.deployed();
        await this.getAdmins();

        let blockNumber = 'latest';
        const deleteEvent = marketPlaceInstance.AdminDeleted({}, {fromBlock: blockNumber});
        const createEvent = marketPlaceInstance.AdminCreated({}, {fromBlock: blockNumber});
        const self = this;
        createEvent.watch(function(error, result){
            if (!error) {
                blockNumber = result.blockNumber + 1;
                console.log("**event-created:::", result);
                self.getAdmins();
            } else {
                console.log("event-error:::", error);
            }
        });
        deleteEvent.watch(function(error, result){
            if (!error) {
                blockNumber = result.blockNumber + 1;
                console.log("**event-deleted:::", result);
                self.getAdmins();
            } else {
                console.log("event-error:::", error);
            }
        });
    }

    async getAdmins() {
        const marketPlaceInstance = await marketPlace.deployed();
        const adminCount = await marketPlaceInstance.getAdminsCount();
        const adminsIndex = await marketPlaceInstance.getAdmins();

        await Promise.all(
            adminsIndex
                .map((element, index) => {
                    return marketPlaceInstance.admins(element);
                })
        ).then(result => this.setState({admins: result}));

        this.setState({
            adminCount: web3.utils.toDecimal(adminCount),
            adminsAddresses: adminsIndex,
            marketPlaceInstance: marketPlaceInstance
        });
    }

    renderRows() {
        return this.state.admins
            .map((element, index) => {
                return (
                    <AdminRow
                        key={index}
                        address={this.state.adminsAddresses[index]}
                        id={web3.utils.toDecimal(element[0])}
                        name={element[1]}
                        error={(message) => {
                            this.setState({errorMessage: message});
                        }}
                    />
                );
            });
    }

    render() {
        const {Header, Row, HeaderCell, Body} = Table;

        return (
            <Form error={!!this.state.errorMessage}>
            <Segment>
                <h3>All admins</h3>
                <Table>
                    <Header>
                        <Row>
                            <HeaderCell>ID</HeaderCell>
                            <HeaderCell>Name</HeaderCell>
                            <HeaderCell>Address</HeaderCell>
                            <HeaderCell></HeaderCell>
                        </Row>
                    </Header>
                    <Body>
                    {this.renderRows()}
                    </Body>
                </Table>
                <div>Found {this.state.adminCount} admins.</div>
                <Message error header="Oops!" content={this.state.errorMessage} />
            </Segment>
            </Form>
        );
    }
}