import React, {Component} from "react";
import {Card, Button, Segment, Header, Container, Loader} from "semantic-ui-react";
import {Link} from 'react-router-dom';
import marketPlace from "../utils/marketplace";
import web3 from "../utils/web3";


export default class HomeIndex extends Component {
    state = {
        storeFrontsCount: 0,
        storeFronts: [],
        marketPlaceInstance : null,
        loading: true
    };

    async componentDidMount() {
        const marketPlaceInstance = await marketPlace.deployed();
        this.setState({marketPlaceInstance: marketPlaceInstance});
        await this.getStoreFronts();
    }

    async getProducts(productKeys) {
        await Promise.all(
            productKeys
                .map(async element => {
                    return await this.state.marketPlaceInstance.products(element);
                })
        ).then(result => {
            productKeys = result;
        });

        return productKeys;
    }

    async createStoreFrontObject(storeFrontId) {
        const {marketPlaceInstance} = this.state;
        let storeFront = await marketPlaceInstance.storeFronts(storeFrontId);
        const storeFrontProducts = await marketPlaceInstance.getProductsByStoreFrontId(storeFrontId);
        if (storeFrontProducts.length > 0) {
            // use only 3 products
            const products = storeFrontProducts.map(product => product.valueOf()).splice(0, 3);
            return {
                id: storeFrontId,
                name: storeFront[2],
                products: await this.getProducts(products)
            };
        }

        return {
            id: storeFrontId,
            name: storeFront[2],
            products: []
        };
    }


    async getStoreFronts() {
        const {marketPlaceInstance} = this.state;
        const storeOwnersAddresses = await marketPlaceInstance.getStoreOwners();
        const result = await Promise.all(
            storeOwnersAddresses
                .map(element => {
                    return marketPlaceInstance.getStoreFrontsByAddress(element);
                })
        );

        let storeFrontIds = [];
        result.map(element => {
            storeFrontIds = storeFrontIds.concat(element);
        });

        const storeFronts = await Promise.all(
            storeFrontIds.map(async storeFrontId => {
                storeFrontId = storeFrontId.valueOf();
                return await this.createStoreFrontObject(storeFrontId)
            })
        );

        this.setState({storeFronts: storeFronts, loading: false});
    }

    renderStoreFronts() {
        return this.state.storeFronts
            .map((storeFront, index) => {
                let items = [];
                storeFront.products.map(product => {
                    items.push({
                        href: '#/products/' + storeFront.id,
                        header: product[2],
                        description: product[3],
                        meta: web3.utils.fromWei(product[4].valueOf(), 'ether') + ' ether',
                    });
                });

                if (items.length == 0) {
                    return null;
                }
                return (
                    <Segment key={index} id={storeFront.id}>
                        <Header as='h2'>{storeFront.name}</Header>
                        <Card.Group items={items} />
                        <Container textAlign='right'>
                                <Button as={Link} size='small' to={`/products/${storeFront.id}`} secondary>
                                    All Products
                                </Button>
                        </Container>

                    </Segment>
                );
            });
    }

    render() {
        if (this.state.loading) {
            return <Loader active inline='centered' />;
        }

        return  (
            <Container>
                {this.renderStoreFronts()}
            </Container>
        );
    }
};