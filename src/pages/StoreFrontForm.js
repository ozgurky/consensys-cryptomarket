import React, { Component } from 'react';
import {Button, Form, Message, Segment} from "semantic-ui-react";
import eventTracker from "../utils/EventTracker";

export default class StoreFrontForm extends Component {
    state = {
        name: '',
        address: '',
        errorMessage: '',
        loading: false
    };

    onSubmit =  async (event) => {
        event.preventDefault();

        this.setState({loading: true, errorMessage: ''});

        try {
            if (this.state.name == '') {
                throw "Please enter name!";
            }

            const createEvent = this.props.contract.StoreFrontCreated({}, {fromBlock: 'latest'});
            const self = this;
            createEvent.watch(function (error, result) {
                if (!error && !eventTracker.checkEvent(result.event, result.transactionHash)) {
                    self.setState({loading: false});
                    self.props.history.push('/store-fronts/list')
                } else {
                    console.log("event-error:::", error);
                }
            });

            await this.props.contract.createStoreFront(
                this.state.name,
                {from: this.props.account}
            );

        } catch (err) {
            let errorMessage;
            errorMessage = err;

            if (typeof err.message != "undefined") {
                errorMessage = err.message;
            }

            this.setState({loading: false, errorMessage: errorMessage});
        }
    };

    render() {
        return (
            <Segment>
                <h3>Create a store front!</h3>
                <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage} loading={this.state.loading}>
                    <Form.Field>
                        <label>Store Front Name</label>
                        <input
                            placeholder='Store front name'
                            value={this.state.name}
                            onChange={event => this.setState({name: event.target.value})}
                        />
                    </Form.Field>
                    <Message error header="Oops!" content={this.state.errorMessage} />
                    <Button primary>Create</Button>
                </Form>
            </Segment>
        );
    }
};