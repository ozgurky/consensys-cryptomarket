import React, {Component} from 'react';
import {Form, Button, Message, Segment} from 'semantic-ui-react';
import web3 from '../utils/web3.js';

export default class AdminForm extends Component {
    state = {
        name: '',
        address: '',
        errorMessage: '',
        loading: false
    };

    onSubmit = async (event) => {
        event.preventDefault();

        this.setState({loading: true, errorMessage: ''});

        try {
            if (this.state.name == '') {
                throw "Please enter name!";
            }

            if (!web3.utils.isAddress(this.state.address)) {
                throw("Please enter a valid address!");
            }

            await this.props.contract.createAdmin(
                this.state.address,
                this.state.name,
                {from: this.props.account}
            );

            const createEvent = this.props.contract.AdminCreated({}, {fromBlock: 'latest'});
            const self = this;
            createEvent.watch(function (error, result) {
                if (!error) {
                    self.props.history.push('/admins/list')
                } else {
                    console.log("event-error:::", error);
                }
            });

        } catch (err) {
            let errorMessage;
            errorMessage = err;

            if (typeof err.message != "undefined") {
                errorMessage = err.message;
            }

            this.setState({errorMessage: errorMessage});
        }
    };

    render() {
        return (
            <Segment>
                <h3>Create an admin!</h3>
                <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage} loading={this.state.loading}>
                    <Form.Field>
                        <label>Admin Name</label>
                        <input
                            placeholder='Admin Name'
                            value={this.state.name}
                            onChange={event => this.setState({name: event.target.value})}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Admin Address</label>
                        <input
                            placeholder='Admin Address'
                            value={this.state.address}
                            onChange={event => this.setState({address: event.target.value})}
                        />
                    </Form.Field>
                    <Message error header="Oops!" content={this.state.errorMessage}/>
                    <Button primary>Create</Button>
                </Form>
            </Segment>
        );
    }
}
