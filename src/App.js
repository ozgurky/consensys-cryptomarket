import React, { Component } from "react";
import marketPlace from "./utils/marketplace";
import web3 from "./utils/web3";
import "./css/App.css";
import { HashRouter as Router} from 'react-router-dom';
import Layout from "./components/Layout";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            contract: null,
            account: null,
            role: [0,0,0,1]
        }
    }

    async componentDidMount() {
        const accounts = await web3.eth.getAccounts();
        const marketPlaceInstance = await marketPlace.deployed();
        const userRole = await marketPlaceInstance.getUserRolesArray({from: accounts[0]});

        this.setState({
            contract: marketPlaceInstance,
            account: accounts[0],
            role: userRole.map(function (item) {
                return web3.utils.toDecimal(item);
            })
        });

        let account = accounts[0];
        self = this;
        const accountInterval = setInterval(async function() {
            const currentAccounts = await web3.eth.getAccounts();
            if (currentAccounts[0] !== account) {
                account = currentAccounts[0];
                window.location.reload();
            }

            const userRoleUpdated = await marketPlaceInstance.getUserRolesArray({from: accounts[0]});
            self.setState({
                role: userRoleUpdated.map((item) => web3.utils.toDecimal(item))
            });
        }, 100);
    }

    render() {
        return (
            <Router>
                <Layout account={this.state.account} contract={this.state.contract} role={this.state.role} />
            </Router>
        );
    }
}

export default App;
