const HDWalletProvider = require("truffle-hdwallet-provider");
const web3 = require("web3");
require('dotenv').config();

module.exports = {
    // See <http://truffleframework.com/docs/advanced/configuration>
    // to customize your Truffle configuration!
    networks: {
        development: {
            host: "127.0.0.1",
            port: "8545",
            network_id: "*"
        },
        rinkeby: {
            provider: function() {
                return new HDWalletProvider(process.env.MNENOMIC, "https://rinkeby.infura.io/v3/" + process.env.INFURA_API_KEY)},
            network_id: 2,
            gasPrice: web3.utils.toWei("20", "gwei"),
            gas: 6700000
        }
    }
};
