#Contract Safety and Security Checklist

## Logic Bugs

To make sure the logic of my contracts I've added several unit tests. I've tried to code TDD pattern as much as possible.
I've checked my codes using solidity-coverage. My project has %100 code coverage.

Following code standards is important for me so I've used some tools like solhint, smartcheck for this reason.

When my base contract has returned too complex, I've changed my design and divided base contract to small parts.

## Integer Arithmetic Overflow

I've used Zeppelin SafeMath library for arithmetic operations.

## Poison Data

I've checked user addresses for all functions and tried to prevent unexpected input data.

## Miner Vulnerabilities

I didn't use any timestamp for my contracts logic. There is only one usage for displaying extra data to user only.

## Malicious Admins

I've implemented Open Zeppelin Pausable and Destructible library to minimize powerful contract administrators risk.

  