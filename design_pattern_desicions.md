# Design Patterns

I've tried to use **Access Restriction** pattern to restrict the access to contract functionality according to suitable criteria.

I've also used **Emergency Stop** and added an option to disable critical contract functionality in case of an emergency. 
Implementing this pattern provides a fail-safe mode for my project.

I've also implemented Open Zeppelin life cycyle contracts to make project destructible (**Mortal pattern**).

**Withdraw pattern** used for protecting against re-entrancy and denial of service attacks.

If I have a bit much time i would use **Checks Effects Interactions** more.




  

