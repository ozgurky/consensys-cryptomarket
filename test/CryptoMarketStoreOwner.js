const CryptoMarketStoreOwner = artifacts.require("CryptoMarketStoreOwner");
const truffleAssert = require('truffle-assertions');

contract('CryptoMarket', async (accounts) => {
    const alice = accounts[0];
    const bob = accounts[1];
    const mark = accounts[2];

    let cryptoMarketStoreOwner;

    beforeEach(async () => {
        cryptoMarketStoreOwner = await CryptoMarketStoreOwner.new();
    });

    describe('Crypto Market Store Owner', () => {
        /**
         * Admin users can create store owners.
         */
        it('should create a store owner', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketStoreOwner.createAdmin(bob, 'Bob');

            await cryptoMarketStoreOwner.createStoreOwner(mark, 'Mark', {from: bob});
            const ownerData = await cryptoMarketStoreOwner.storeOwners(mark);

            assert.equal("Mark", ownerData[1], 'First store owner\'s name was not Bob');
        });

        /**
         * Only an admin user can create store owner, so trying with non admin user should throw an error.
         */
        it('should create a store owner by only admin', async () => {
            let result = false;
            try {
                await cryptoMarketStoreOwner.createStoreOwner(bob, 'Bob', {from: mark});
            } catch (err) {
                result = true;
            }

            assert(result);
        });

        /**
         * Store owners are address unique records, so they can be only created once.
         */
        it('should create same store owner only once', async() => {
            await cryptoMarketStoreOwner.createAdmin(bob, 'Bob');
            await cryptoMarketStoreOwner.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreOwner.createStoreOwner(mark, 'Mark', {from: bob});
            const ownersCount = await cryptoMarketStoreOwner.getStoreOwnersCount();

            assert.equal("1", ownersCount.toString(), 'Same owner address can be created twice');
        });

        /**
         * A store owner can be deleted by using its address
         */
        it('should delete a store owner with address', async() => {
            await cryptoMarketStoreOwner.createAdmin(bob, 'Bob');
            await cryptoMarketStoreOwner.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreOwner.createStoreOwner(alice, 'Alice', {from: bob});

            await cryptoMarketStoreOwner.deleteStoreOwner(mark, {from: bob});
            const ownerCount = await cryptoMarketStoreOwner.getStoreOwnersCount();
            const ownerData = await cryptoMarketStoreOwner.storeOwners(mark);

            assert(!ownerData[2], 'Mark\'s store owner data status was not false')
            assert.equal("1", ownerCount.toString(), 'Store owners count was not 1');
        });

        /**
         * All store owners's addresses can be retrieved
         */
        it('should get addresses of store owners', async() => {
            await cryptoMarketStoreOwner.createAdmin(bob, 'Bob');
            await cryptoMarketStoreOwner.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreOwner.createStoreOwner(alice, 'Alice', {from: bob});

            const storeOwners = await cryptoMarketStoreOwner.getStoreOwners();

            assert.equal(2, storeOwners.length, 'Store owners count was not 2')
            assert.equal(mark, storeOwners[0], 'First store owners address was not Mark\'s address');
        });

        /**
         * Creating a store owner should trigger an event and event data should includes its data.
         */
        it('should fire an event when a new store owner created', async() => {
            await cryptoMarketStoreOwner.createAdmin(bob, 'Bob');
            const result = await cryptoMarketStoreOwner.createStoreOwner(mark, 'Mark', {from: bob});

            truffleAssert.eventEmitted(result, 'StoreOwnerCreated', (event) => {
                return event.storeOwnerAddress === mark && event.name === 'Mark';
            });
        });

        /**
         * Deleting a store owner should trigger an event and event data should includes its data.
         */
        it('should fire an event when a store owner deleted', async() => {
            await cryptoMarketStoreOwner.createAdmin(bob, 'Bob');
            await cryptoMarketStoreOwner.createStoreOwner(mark, 'Mark', {from: bob});
            const result = await cryptoMarketStoreOwner.deleteStoreOwner(mark, {from: bob});

            truffleAssert.eventEmitted(result, 'StoreOwnerDeleted', (event) => {
                return event.storeOwnerAddress === mark && event.name === 'Mark';
            });
        });
    });
});
