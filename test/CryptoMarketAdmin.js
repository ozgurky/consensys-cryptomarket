const CryptoMarketAdmin = artifacts.require("CryptoMarketAdmin");
const truffleAssert = require('truffle-assertions');

contract('CryptoMarket', async (accounts) => {
    const bob = accounts[1];
    const mark = accounts[2];

    let cryptoMarketAdmin;

    beforeEach(async () => {
        cryptoMarketAdmin = await CryptoMarketAdmin.new();
    });

    describe('Crypto Market Admin', () => {
        /**
         * Owner can create admin and in this test app adds an admin whose name is Bob.
         */
        it('should create an admin', async () => {
            await cryptoMarketAdmin.createAdmin(bob, 'Bob');
            const adminData = await cryptoMarketAdmin.admins(bob);

            assert.equal("Bob", adminData[1], 'First admin name was not Bob');
        });

        /**
         * Only super admin can create an admin so when another account try to create this app should throw error.
         */
        it('should create an admin by only super admin', async () => {
            let result = false;
            try {
                await cryptoMarketAdmin.createAdmin(bob, 'Bob', {from: mark});
            } catch (err) {
                result = true;
            }

            assert(result);
        });

        /**
         * Admins are stored in chain using their addresses, so every address can only be added once
         * App tries to same address twice and it wont meet any error but admins count wont change.
         */
        it('should create same admin only once', async() => {
            await cryptoMarketAdmin.createAdmin(bob, 'Bob');
            await cryptoMarketAdmin.createAdmin(bob, 'Bob');
            const adminCount = await cryptoMarketAdmin.getAdminsCount();

            assert.equal("1", adminCount.toString(), 'Same address can be created twice');
        });

        /**
         * Deleting admin will cause a decrement on admins count and also admin struct value of given address will return
         * default values of every type.
         */
        it('should delete an admin with address', async() => {
            await cryptoMarketAdmin.createAdmin(bob, 'Bob');
            await cryptoMarketAdmin.createAdmin(mark, 'Mark');
            await cryptoMarketAdmin.deleteAdmin(bob)
            const adminCount = await cryptoMarketAdmin.getAdminsCount();
            const adminData = await cryptoMarketAdmin.admins(bob);

            assert(!adminData[2], 'Bob admin data status was not false')
            assert.equal("1", adminCount.toString(), 'Admin count was not 1');
        });

        /**
         * Reaching list of all admin addresses can be useful.
         */
        it('should get list of admin addresses', async() => {
            await cryptoMarketAdmin.createAdmin(bob, 'Bob');
            await cryptoMarketAdmin.createAdmin(mark, 'Mark');

            const admins = await cryptoMarketAdmin.getAdmins();

            assert.equal(mark, admins[1], 'Second admin\'s address was not Mark\'s address');
            assert.equal("2", admins.length, 'Admin count was not 2');
        });

        /**
         * Creating admin should fire an event app can read created admin data using this event.
         */
        it('should fire an event when a new admin created', async() => {
            const result = await cryptoMarketAdmin.createAdmin(bob, 'Bob');

            truffleAssert.eventEmitted(result, 'AdminCreated', (event) => {
                return event.adminAddress === bob && event.name === 'Bob';
            });
        });

        /**
         * Deleting admin should fire an event app can read delete admin data using this event.
         */
        it('should fire an event when an admin deleted', async() => {
            await cryptoMarketAdmin.createAdmin(bob, 'Bob');
            const result = await cryptoMarketAdmin.deleteAdmin(bob);

            truffleAssert.eventEmitted(result, 'AdminDeleted', (event) => {
                return event.adminAddress === bob && event.name === 'Bob';
            });
        });

    });
});
