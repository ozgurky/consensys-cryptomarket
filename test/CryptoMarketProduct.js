const CryptoMarketProduct = artifacts.require("CryptoMarketProduct");
const truffleAssert = require('truffle-assertions');

contract('CryptoMarket', async (accounts) => {
    const alice = accounts[0];
    const bob = accounts[1];
    const mark = accounts[2];

    let cryptoMarketProduct;

    beforeEach(async () => {
        cryptoMarketProduct = await CryptoMarketProduct.new();
    });

    describe('Crypto Market Product', () => {
        /**
         * Only store owner can create product but products should be assigned to store fronts so we should create store
         * fronts. Every product will give an unique index.
         */
        it('should create a product', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketProduct.createProduct(
                1,
                'my product',
                'my product desc',
                1000,
                10,
                true,
                {from: mark}
            );

            await cryptoMarketProduct.createProduct(
                1,
                'my product 2',
                'my product 2 desc',
                10000,
                10,
                true,
                {from: mark}
            );

            const productData = await cryptoMarketProduct.products(1);
            const productData2 = await cryptoMarketProduct.products(2);

            assert.equal("my product", productData[2], 'First product\'s name was not "my product"');
            assert.equal(2, productData2[0], 'Second product\'s id was not "2"');
        });


        /**
         * If an user tries to create a product and given Store Front is not user's store front app will throw an error
         */
        it('should throw an error when store front does not belong to sender', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreOwner(alice, 'Alice', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            let result = false;
            try {
                await cryptoMarketProduct.createProduct(
                    1,
                    'my product',
                    'my product desc',
                    1000,
                    10,
                    true,
                    {from: alice}
                );
            } catch (err) {
                result = true;
            }

            assert(result, 'User could create a product into a store front which was not belong to her');
        });


        /**
         * Products in CryptoMarket are updatable, so app could update a product with its id.
         */
        it('should update a product', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketProduct.createProduct(
                1,
                'my product',
                'my product desc',
                1000,
                10,
                true,
                {from: mark}
            );

            await cryptoMarketProduct.updateProduct(
                1,
                'my product updated',
                'my product desc',
                1000,
                10,
                true,
                {from: mark}
            );

            const productData = await cryptoMarketProduct.products(1);

            assert.equal("my product updated", productData[2], 'First product\'s name was not "my product updated"');
        });

        /**
         * Every store owner only can update his/her product.
         */
        it('should throw an error when trying to update a product which is not created by store owner', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreOwner(alice, 'Alice', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketProduct.createProduct(
                1,
                'my product',
                'my product desc',
                1000,
                10,
                true,
                {from: mark}
            );

            let result = false;
            try {
                await cryptoMarketProduct.updateProduct(
                    1,
                    'my product updated',
                    'my product desc',
                    1000,
                    10,
                    true,
                    {from: alice}
                );
            } catch (err) {
                result = true;
            }

            assert(result, "User could update a product which was not belong to her")
        });

        /**
         * A product can be deleted by its creator only.
         */
        it('should throw an error when trying to delete other store owner\'s product', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreOwner(alice, 'Alice', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 1', 'some desc', 1000, 10, true, {from: mark});

            let result = false;
            try {
                await cryptoMarketProduct.deleteProduct(1, {from: alice, gas: 1000000});
            } catch (err) {
                result = true;
            }

            assert(result, "Product could be deleted any store owner");
        });

        /**
         * Products can be deleted by ids. After deleting a product product struct data shoul return its default value.
         */
        it('should delete a product by id', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 1', 'some desc', 1000, 10, true, {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 2', 'some desc', 1000, 10, true, {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 3', 'some desc', 1000, 10, true, {from: mark});

            await cryptoMarketProduct.deleteProduct(2, {from: mark, gas: 1000000});


            const deletedProduct = await cryptoMarketProduct.products(2);

            assert.equal('', deletedProduct[2], 'Product 2 could not be deleted');
        });

        /**
         * It should return all product ids assigned to the given store front id
         */
        it('should get product ids of given store front', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 1', 'some desc', 1000, 10, true, {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 2', 'some desc', 1000, 10, true, {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 3', 'some desc', 1000, 10, true, {from: mark});



            const products = await cryptoMarketProduct.getProductsByStoreFrontId(1);

            assert.equal('2', products[1], 'Second product id was not 2');
            assert.equal('3', products.length, 'Store Front did not have 3 products');
        });




        /**
         * Creating a product should trigger an event and event data should includes product data.
         */
        it('should fire an event when a new product created', async() => {
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            const result = await cryptoMarketProduct.createProduct(1, 'my product 1', 'some desc', 1000, 10, true, {from: mark});

            truffleAssert.eventEmitted(result, 'ProductCreated', (event) => {
                return event.ownerAddress === mark && event.name === 'my product 1' && event.id.toNumber() === 1;
            });
        });

        /**
         * Updating a product should trigger an event and event data should includes product data.
         */
        it('should fire an event when a product updated', async() => {
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 1', 'some desc', 1000, 10, true, {from: mark});
            const result = await cryptoMarketProduct.updateProduct(1, 'my updated product 1', 'some desc', 1000, 10, true, {from: mark});

            truffleAssert.eventEmitted(result, 'ProductUpdated', (event) => {
                return event.ownerAddress === mark && event.name === 'my updated product 1' && event.id.toNumber() === 1;
            });
        });

        /**
         * Deleting a product should trigger an event and event data should includes product id.
         */
        it('should fire an event when a product deleted', async() => {
            await cryptoMarketProduct.createAdmin(bob, 'Bob');
            await cryptoMarketProduct.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketProduct.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketProduct.createProduct(1, 'my product 1', 'some desc', 1000, 10, true, {from: mark});

            const result = await cryptoMarketProduct.deleteProduct(1, {from: mark, gas: 1000000});

            truffleAssert.eventEmitted(result, 'ProductDeleted', (event) => {
                return event.productId.toNumber() === 1;
            });
        });
    });
});
