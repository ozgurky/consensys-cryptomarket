const CryptoMarket = artifacts.require("CryptoMarket");
const Helper = require("../src/utils/helpers");

contract('CryptoMarket', async (accounts) => {
    const alice = accounts[0];
    const bob = accounts[1];
    const mark = accounts[2];

    let cryptoMarket;

    beforeEach(async () => {
        cryptoMarket = await CryptoMarket.new();
    });

    describe('Crypto Market Role', () => {
        /**
         * Only creator of contract (in this case fist account of current accounts) should be super admin (owner).
         */
        it('should check if user is super admin', async() => {
            assert(await cryptoMarket.isSuperAdmin());
            assert(!await cryptoMarket.isSuperAdmin({from: mark}));
        });

        /**
         * We first create an admin using default account (owner) and then checks if created account has an admin role.
         * We should check any other account does not have the admin role.
         */
        it('should check if user is admin', async() => {
            await cryptoMarket.createAdmin(bob, 'Bob');

            assert(!await cryptoMarket.isAdmin());
            assert(await cryptoMarket.isAdmin({from: bob}));
        });

        /**
         * Admins can create store owner and it is possible to check an address is store owner address.
         */
        it('should check if user is a store owner', async() => {
            await cryptoMarket.createAdmin(bob, 'Bob');
            await cryptoMarket.createStoreOwner(mark, 'Mark', {from: bob});

            assert(!await cryptoMarket.isStoreOwner({from: bob}));
            assert(await cryptoMarket.isStoreOwner({from:  mark}));
        });

        /**
         * Owner of contract can be also admin, store owner and user same time.
         */
        it('should check if user has all roles ', async() => {
            await cryptoMarket.createAdmin(alice, 'Alice');
            await cryptoMarket.createStoreOwner(alice, 'Alice', {from: alice});

            let userRoleAlice = await cryptoMarket.getUserRolesArray();
            userRoleAlice = userRoleAlice.map(function (item) {
                return web3.toDecimal(item);
            });

            assert(Helper.isSuperAdmin(userRoleAlice), "Alice's role was not super admin.");
            assert(Helper.isAdmin(userRoleAlice), "Alice's role was not admin.");
            assert(Helper.isStoreOwner(userRoleAlice), "Alice's role was not store owner.");
            assert(Helper.isUser(userRoleAlice), "Alice's role was not user.");
        });

        /**
         * An user can have multiple roles, so we can check if default user has super admin and user role.
         */
        it('should get user role', async() => {
            let userRoleAlice = await cryptoMarket.getUserRolesArray();
            userRoleAlice = userRoleAlice.map(function (item) {
                return web3.toDecimal(item);
            });
            assert(Helper.isSuperAdmin(userRoleAlice), "Alice's role was not super admin.");
            assert(!Helper.isAdmin(userRoleAlice), "Alice's role was admin.");
            assert(!Helper.isStoreOwner(userRoleAlice), "Alice's role was store owner.");
            assert(Helper.isUser(userRoleAlice), "Alice's role was not user.");
        });


        /**
         * An user can have multiple roles, so we can check if a random user has only user role.
         */
        it('should get user role', async() => {
            let userRoleMark = await cryptoMarket.getUserRolesArray({from: mark});
            userRoleMark = userRoleMark.map(function (item) {
                return web3.toDecimal(item);
            });

            assert.deepEqual([0, 0, 0, 1], userRoleMark, "Mark's roles array was not correct");
        });
    });
});
