const CryptoMarketTransaction = artifacts.require("CryptoMarketTransaction");
const truffleAssert = require('truffle-assertions');
const Web3 = require("web3");
const web3Latest = new Web3(web3.currentProvider);

contract('CryptoMarket', async (accounts) => {
    const alice = accounts[0];
    const bob = accounts[1];
    const mark = accounts[2];

    let cryptoMarketTransaction;

    beforeEach(async () => {
        cryptoMarketTransaction = await CryptoMarketTransaction.new();
    });

    describe('Crypto Market Transaction', () => {
        /**
         * A product can be purchased transferring its exact price to contract.
         */
        it('should buy a product with exact value', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', 1500, 10, true, {from: mark});

            await cryptoMarketTransaction.buyProduct(1, {from: alice, value:1500});

            const product = await cryptoMarketTransaction.products(1);
            const markBalance = await cryptoMarketTransaction.getSenderBalance({from: mark});

            assert.equal(9, product[5], 'Product quantity was not 9');
            assert.equal(1500, markBalance, 'Mark\'s balance was not 1500');
        });

        /**
         * A product can not be purchased when msg.value is not enough.
         */
        it('should throw an error if value insufficient', async () => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', 1500, 10, true, {from: mark});

            let result = false;
            try {
                await cryptoMarketTransaction.buyProduct(1, {from: alice, value:1400});
            } catch (err) {
                result = true;
            }

            assert(result, "User could purchase th product with insufficient value");
        });

        /**
         * When an user try to buy a product transferring a bigger amount than its price, remaining ether should refund
         * the buyers address
         */
        it('should buy a product with bigger value & and return extra', async () => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', 1500, 10, true, {from: mark});

            await cryptoMarketTransaction.buyProduct(1, {from: alice, value:15000});

            const product = await cryptoMarketTransaction.products(1);
            const markBalance = await cryptoMarketTransaction.getSenderBalance({from: mark});
            const contractBalance = await web3Latest.eth.getBalance(cryptoMarketTransaction.address);

            assert.equal(9, product[5], 'Product quantity was not 9');
            assert.equal(1500, markBalance, 'Mark\'s balance was not 1500');
            assert.equal(1500, contractBalance.valueOf(), 'Contract balance was not 0');
        });

        /**
         * Every transaction should be recorded and buyers & sellers can view their alt transactions.
         */
        it('should buy a product and save details to transaction', async () => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', 1500, 10, true, {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 2', 'some desc', 1500, 10, true, {from: mark});

            await cryptoMarketTransaction.buyProduct(1, {from: alice, value:1500});
            await cryptoMarketTransaction.buyProduct(2, {from: alice, value:1500});

            const transaction = await cryptoMarketTransaction.transactions(1);
            const keys = ['id', 'productId', 'productName', 'buyer', 'seller', 'price', 'quantity', 'date'];
            let transactionObject = {};
            keys.map((key, index) => {
                transactionObject[key] = transaction[index].valueOf();
            });

            const aliceTransactions = await cryptoMarketTransaction.getPurchasedProductsAsBuyer({from: alice});
            const markTransactions = await cryptoMarketTransaction.getPurchasedProductsAsSeller({from: mark});

            assert.equal(mark, transactionObject.seller, "Transaction seller was not Mark.");
            assert.equal(alice, transactionObject.buyer, "Transaction buyer was not Alice.");
            assert.equal(2, aliceTransactions.length, "Alice\'s transaction count was not 1.");
            assert.equal(2, markTransactions.length, "Mark\'s transaction count was not 1.");
        });

        /**
         * Purchasing a product will descrease buyer balance because product price will be transferred to contract.
         */
        it('should cause a decrease on buyer balance', async () => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', web3Latest.utils.toWei('0.1', 'ether'), 10, true, {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 2', 'some desc', web3Latest.utils.toWei('0.2', 'ether'), 10, true, {from: mark});


            const aliceInitialBalance = await web3Latest.eth.getBalance(alice);
            await cryptoMarketTransaction.buyProduct(1, {from: alice, value: web3Latest.utils.toWei('0.1', 'ether')});
            await cryptoMarketTransaction.buyProduct(2, {from: alice, value: web3Latest.utils.toWei('0.2', 'ether')});

            const aliceFinalBalance = await web3Latest.eth.getBalance(alice);
            const difference = aliceInitialBalance - aliceFinalBalance;

            assert(difference > web3Latest.utils.toWei('0.3', 'ether'));
        });

        /**
         * Total amount of products price sold by store owner can be withdrawn.
         */
        it('should withdraw from balance', async () => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', web3Latest.utils.toWei('0.1', 'ether'), 10, true, {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 2', 'some desc', web3Latest.utils.toWei('0.2', 'ether'), 10, true, {from: mark});


            const markInitialBalance = await web3Latest.eth.getBalance(mark);
            await cryptoMarketTransaction.buyProduct(1, {from: alice, value: web3Latest.utils.toWei('0.1', 'ether')});
            await cryptoMarketTransaction.buyProduct(2, {from: alice, value: web3Latest.utils.toWei('0.2', 'ether')});


            await cryptoMarketTransaction.withdraw({from: mark});

            const markFinalBalance = await web3Latest.eth.getBalance(mark);
            const difference = markFinalBalance - markInitialBalance;

            const markBalanceOnContract = await cryptoMarketTransaction.getSenderBalance({from: mark});
            assert.equal(0, markBalanceOnContract.valueOf(), "Mark balance was not 0 on contract");
            assert(difference > web3Latest.utils.toWei('0.28', 'ether'), 'Diffence was not bigger than 0.28 ether' + difference);
        });

        /**
         * Only store owner who has balance can withdraw.
         */
        it('should thow an error when store owner try to withdraw with zero balance', async () => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});

            let result = false;
            try {
                await cryptoMarketTransaction.withdraw({from: mark});
            } catch (err) {
                result = true;
            }

            assert(result, "Store owner with zero balance could withdraw");
        });


        it('should fire an event when a new product sold', async() => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', web3Latest.utils.toWei('0.1', 'ether'), 10, true, {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 2', 'some desc', web3Latest.utils.toWei('0.2', 'ether'), 10, true, {from: mark});

            const result = await cryptoMarketTransaction.buyProduct(2, {from: alice, value: web3Latest.utils.toWei('0.2', 'ether')});

            truffleAssert.eventEmitted(result, 'ProductSold', (event) => {
                return event.id.toNumber() === 2;
            });
        });

        /**
         * Withdrawing should trigger an event.
         */
        it('should fire an event when store owner withdraw her balance', async() => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', web3Latest.utils.toWei('0.1', 'ether'), 10, true, {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 2', 'some desc', web3Latest.utils.toWei('0.2', 'ether'), 10, true, {from: mark});

            await cryptoMarketTransaction.buyProduct(2, {from: alice, value: web3Latest.utils.toWei('0.2', 'ether')});
            const result = await cryptoMarketTransaction.withdraw({from: mark});

            truffleAssert.eventEmitted(result, 'BalanceWithdrawn', (event) => {
                return event.owner ===  mark && event.amount.valueOf() === web3Latest.utils.toWei('0.2', 'ether');
            });
        });

        /**
         * When contract paused as an emergency stop withdrawing cannot be possible.
         */
        it('should prevent withdraw and buy actions when contract is paused', async() => {
            await cryptoMarketTransaction.createAdmin(bob, 'Bob');
            await cryptoMarketTransaction.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketTransaction.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketTransaction.createProduct(1, 'my product 1', 'some desc', web3Latest.utils.toWei('0.1', 'ether'), 10, true, {from: mark});

            await cryptoMarketTransaction.pause();
            let result = false;
            try {
                await cryptoMarketTransaction.buyProduct(2, {from: alice, value: web3Latest.utils.toWei('0.2', 'ether')});
                await cryptoMarketTransaction.withdraw({from: mark});
            } catch (error) {
                result = true;
            }

            assert(result);
        });
    });
});
