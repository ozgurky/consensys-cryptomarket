const CryptoMarketStoreFront = artifacts.require("CryptoMarketStoreFront");
const truffleAssert = require('truffle-assertions');

contract('CryptoMarket', async (accounts) => {
    const alice = accounts[0];
    const bob = accounts[1];
    const mark = accounts[2];

    let cryptoMarketStoreFront;

    beforeEach(async () => {
        cryptoMarketStoreFront = await CryptoMarketStoreFront.new();
    });

    describe('Crypto Markset Store Front', () => {
        /**
         * Only store owners can create store front and only admin can create store owner, so first of test app should
         * create an admin and then a store owner. After that it is possible to create a store owner.
         */
        it('should create a store front', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketStoreFront.createAdmin(bob, 'Bob');
            await cryptoMarketStoreFront.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreFront.createStoreFront('My Store Front', {from: mark});
            const storeFrontData = await cryptoMarketStoreFront.storeFronts(1);

            assert.equal("My Store Front", storeFrontData[2], 'First store front\'s name was not "My Store Front"');
        });

        /**
         * Store fronts can only be created store owners when a non admin user try to create a store front app should
         * throw an error
         */
        it('should create a store front only by a store owner', async () => {
            let result = false;
            try {
                await cryptoMarketStoreFront.createStoreFront('Store front', {from: mark});
            } catch (err) {
                result = true;
            }

            assert(result);
        });

        /**
         * A store front can be updated.
         */
        it('should update a store front', async () => {
            await cryptoMarketStoreFront.createAdmin(bob, 'Bob');
            await cryptoMarketStoreFront.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreFront.createStoreFront('My Store Front', {from: mark});
            await cryptoMarketStoreFront.updateStoreFront(1, 'My Updated Store Front', {from: mark});

            const storeFrontData = await cryptoMarketStoreFront.storeFronts(1);

            assert.equal("My Updated Store Front", storeFrontData[2], 'First store front\'s name was not updated.');
        });

        /**
         * A store front can be updated by its owner only.
         */
        it('should throw an error when an user try to update store front which is not belong to her', async () => {
            await cryptoMarketStoreFront.createAdmin(bob, 'Bob');
            await cryptoMarketStoreFront.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreFront.createStoreOwner(alice, 'Alice', {from: bob});
            await cryptoMarketStoreFront.createStoreFront('My Store Front', {from: mark});
            let result = false;
            try {
                await cryptoMarketStoreFront.updateStoreFront(1, 'My Updated Store Front', {from: alice});
            } catch (err) {
                result = true;
            }

            assert(result, "Alice could update Mark's store front");
        });


        /**
         * It should be possible to get all store fronts using a store owner address
         */
        it('should get all store fronts', async () => {
            //only admins can create store owner so we should create an admin first
            await cryptoMarketStoreFront.createAdmin(bob, 'Bob');
            await cryptoMarketStoreFront.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreFront.createStoreFront('My Store Front 1', {from: mark});
            await cryptoMarketStoreFront.createStoreFront('My Store Front 2', {from: mark});

            const storeFrontKeysMark = await cryptoMarketStoreFront.getStoreFrontsByAddress(mark);
            const storeFrontKeysAlice = await cryptoMarketStoreFront.getStoreFrontsByAddress(alice);

            assert.equal("2", storeFrontKeysMark.length, 'Mark\'s store fronts count was not 2.');
            assert.equal("0", storeFrontKeysAlice.length, 'Alice\'s store fronts count was not 0.');
        });

        /**
         * All store owners's addresses can be retrieved
         */
        it('should get ids of sender store fronts ', async() => {
            await cryptoMarketStoreFront.createAdmin(bob, 'Bob');
            await cryptoMarketStoreFront.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreFront.createStoreFront('My Store Front 1', {from: mark});
            await cryptoMarketStoreFront.createStoreFront('My Store Front 2', {from: mark});

            const storeFrontIds = await cryptoMarketStoreFront.getStoreFrontsByOwner({from: mark});

            assert.equal("2", storeFrontIds.length, 'Mark\'s store fronts count was not 2.');
            assert.equal(2, storeFrontIds[1], 'Mark\'s  second store front id was not 2.');
        });

        /**
         * Creating a store front should trigger an event and event data should includes store front data.
         */
        it('should fire an event when a new store front created', async() => {
            await cryptoMarketStoreFront.createAdmin(bob, 'Bob');
            await cryptoMarketStoreFront.createStoreOwner(mark, 'Mark', {from: bob});
            const result = await cryptoMarketStoreFront.createStoreFront('My Store Front', {from: mark});

            truffleAssert.eventEmitted(result, 'StoreFrontCreated', (event) => {
                return event.storeOwnerAddress === mark && event.name === 'My Store Front' && event.id.toNumber() === 1;
            });
        });

        /**
         * Updating a store front should trigger an event and event data should includes its data.
         */
        it('should fire an event when a store front updated', async() => {
            await cryptoMarketStoreFront.createAdmin(bob, 'Bob');
            await cryptoMarketStoreFront.createStoreOwner(mark, 'Mark', {from: bob});
            await cryptoMarketStoreFront.createStoreFront('My Store Front', {from: mark});
            const result = await cryptoMarketStoreFront.updateStoreFront(1, 'My Updated Store Front', {from: mark});

            truffleAssert.eventEmitted(result, 'StoreFrontUpdated', (event) => {
                return event.name === 'My Updated Store Front' && event.id.toNumber() === 1;
            });
        });
    });
});
